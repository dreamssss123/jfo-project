
@extends("backend/layouts/backend")


@push('add_css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}">
<style>
.dataTables_filter{display: none;}
</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Edit Partner</h2>
</div>

<div class="card mb-3">
	<div class="card-body">

		@include('layouts.errors')

		<form method="post" action="javascript:" id="form_partner">
			@csrf
			
			@include('backend.partner.form')
		</form>

	</div>
</div>

<!-- {{--<div class="card mb-3">
	<div class="card-body">
		@include('backend.partner.rest_table')
	</div>
</div>--}} -->

<div>
	<div class="text-right">
		<a href="{{ route('backend.partner.index') }}" class="btn btn-secondary btn_form">Cancel</a>
		&nbsp;
		<button type="submit" class="btn btn_pink btn_form btn_partner_default">Save</button>
		<button type="submit" class="btn btn_pink btn_form btn_partner_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
	</div>
</div>
@endsection


@push('add_js')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('../vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side.js') }}"></script> -->
{!! $dataTable->scripts() !!}
@endpush
