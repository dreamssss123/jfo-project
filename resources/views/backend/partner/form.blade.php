

@push('add_css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}">
<style>
#tb_api td{padding:5px;}
.remove_api{padding:0; width:20px; height:20px; line-height: 16px; border:solid 1px #ccc; color:#fff; background:red; cursor:pointer;}
</style>
@endpush

<div id="ajax_validate"></div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Partner name : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" class="form-control" name="par_name" value="{{ @$partner->par_name }}">
    </div>
</div>


<?php
$cates = \App\ApiCategory::where('status', 1)->get();
?>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Partner organization : <span class="req">*</span></label>
    <div class="col-sm-10">
        <select class="form-control" name="category_id">
            <option value>Select Partner organization</option>
            <?php
            foreach( $cates as $cat ){

                $selected = @$partner->category_id==$cat->id ? 'selected':'';

                echo "<option value='".$cat->id."' ".$selected.">".$cat->cat_name."</option>";
            }
            ?>
        </select>
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label">Personal contact : <span class="req">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="par_person_contact" value="{{ @$partner->par_person_contact }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Email : <span class="req">*</span></label>
    <div class="col-sm-10">
        <input type="email" class="form-control" name="par_email" value="{{ @$partner->par_email }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Username : <span class="req">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="username" value="{{ @$partner->username }}" {{ @$partner->id ? 'disabled':'' }}>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Password : <span class="req">*</span></label>
    <div class="col-sm-10">
        <?php $u = new \App\User;?>
        <input type="password" class="form-control alias_str" name="password" value="{{ @$partner->id ? $u->blank_password:'' }}">
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label">Department : </label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="par_department" value="{{ @$partner->par_department }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Mobile / Tel. : <span class="req">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="par_mobile" value="{{ @$partner->par_mobile }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Address. : </label>
    <div class="col-sm-10">
        <textarea class="form-control" rows="3" name="par_address">{{ @$partner->par_address }}</textarea>
    </div>
</div>




<div class="form-group row label_status mb-5">
	<label class="col-sm-2 col-form-label">Status : </label>
	<div class="col-sm-10">
		<?php
		$checked = 1;
		if( isset($partner->status) && $partner->status == 0 )
			$checked = 0;
		?>
    	<label>
    		<input type="radio" name="status" value="1" {{ $checked==1? 'checked':'' }}>
    		Enable
    	</label>
    	&nbsp;
    	<label>
    		<input type="radio" name="status" value="0" {{ $checked==0? 'checked':'' }}>
    		Disabled
    	</label>
    </div>
</div>


<h2>Search API</h2>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">API name. : </label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="search_api">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label"></label>
    <div class="col-sm-10">
        <table id="tb_api" class="table table-bordered">
            <tbody id="api_choose">
                <?php
                if( isset( $partner ) && count($partner->api_map) > 0 ){
                    foreach( $partner->api_map as $map ){
                        if( $map->api ){
                            echo "<tr>";
                            echo    "<td id='td_api_".$map->api->id."'>";
                            echo        "<button type='button' class='remove_api'><i class='fa fa-times'></i></button> ";
                            echo        "&nbsp; <span>".$map->api->api_name."<span>";
                            echo        "<input type='hidden' name='api_id[]' value='".$map->api->id."'>";
                            echo    "</td>";
                            echo "</tr>";
                        }
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>




@push('add_js')
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<script>
$(document).ready(function(){

<?php
$has_id = @$partner->id;
if( !$has_id )
    $has_id = 'no_id';
?>
$("#search_api").autocomplete({
    source: function (request, response) {
        jQuery.get("{{ route('backend.partner.list_api', $has_id) }}", {
            query: request.term
        }, (data) => {
            // console.log(data);
            response($.map(data, function (pn) {
                return {
                    value: pn.id,
                    label: pn.api_name
                };
            }));
        });
    },
    select: function (event, ui)
    {
        // console.log(ui.item);
        if( $("#api_choose #td_api_"+ui.item.value).length == 0 ){
            let api_choose = '';
            api_choose += "<tr>";
            api_choose +=   "<td id='td_api_"+ui.item.value+"'>";
            api_choose +=       "<button type='button' class='remove_api'><i class='fa fa-times'></i></button> ";
            api_choose +=       "&nbsp; <span>"+ui.item.label+"</span>";
            api_choose +=       "<input type='hidden' name='api_id[]' value='"+ui.item.value+"'>";
            api_choose +=   "</td>";
            api_choose += "</tr>";
            $("#api_choose").append(api_choose);
        }

        $("#search_api").val('');
        return false;
    }
});

$("body").on('click', '.remove_api', function(){
    $(this).parent().parent().remove();
});


<?php
if( @$partner->id != '' )
    $url = route('backend.partner.update', $partner->id);
else
    $url = route('backend.partner.store');
?>
$('.btn_partner_default').click(function(){

    $('.btn_partner_default').hide();
    $('.btn_partner_loading').show();

    var formData = new FormData($("#form_partner")[0]);
    @if( @$partner->id != '' )
    formData.append('_method', 'PUT');
    @endif
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ $url }}",
        type: "post",
        data: formData,
        processData: false,
        contentType: false,
        success: function(res)
        {
            // console.log(res);
            $('.btn_partner_default').show();
            $('.btn_partner_loading').hide();

            if( res.status == 'success' ){
                swal({
                    type: "success",
                    title: 'Success',
                    text: 'Created.',
                })
                .then(function(res){
                    @if( @$partner->id != '' )
                    window.location.reload();
                    @else
                    window.location.href = "{{ route('backend.partner.index') }}";
                    @endif
                });

            }else{
                var html = html_alert(res.data);
                $('#ajax_validate').html(html);
                $("html, body").animate({ scrollTop: 0 }, "fast");
            }

        },
        error: function (jqXHR, exception)
        {
            $('.btn_partner_default').show();
            $('.btn_partner_loading').hide();
            let msg = set_ajax_error(jqXHR, exception);
            swal({
                type: 'error',
                title: 'Error',
                text: msg,
            });
        },
    });

    return false;
});

});

</script>
@endpush
