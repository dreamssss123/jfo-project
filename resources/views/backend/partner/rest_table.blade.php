

<div class="row">
	<div class="col-md-3">
		<input type="text" class="form-control" id="search_name" placeholder="Search API name">
	</div>

	<div class="col-md-3">
		<input type="text" class="form-control" id="search_url" placeholder="Search API URL">
	</div>
</div>

<div class="table-responsive">
	{!! $dataTable->table(['id'=> 'tb_main', 'class'=> 'table table-bordered table-hover wid100']) !!}
</div>