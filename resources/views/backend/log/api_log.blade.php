
@extends("backend/layouts/backend")


@push('add_css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bootstrap4-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css') }}">

<style>
.dataTables_filter{display: none;}
</style>
@endpush

@section('content')
<?php
$perms = \App\Perm::can();
?>

<div class="section-header">
	<h2>
		@if( @request()->input('log_type') == 'error' )
		API Error log
		@else
		API log
		@endif
		<form method="post" action="{{ route('backend.apilog.export_data', 'csv') }}" id="export_logs" class="pull-right">
			@csrf
			<input type="hidden" name="seach_name">
			<input type="hidden" name="seach_partner">
			<input type="hidden" name="search_status">
			<input type="hidden" name="search_date_start">
			<input type="hidden" name="search_date_end">
			@if( @request()->input('log_type') == 'error' )
			<input type="hidden" name="not_status_200" value="1">
			@endif
			<button type="submit" class="btn btn_pink btn-sm" style="width:100px;">Export</button>
		</form>
	</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<div class="row" id="filter_box">
			<div class="col-md-3">
				<input type="text" class="form-control" id="seach_name" placeholder="API URL">
			</div>

			<div class="col-md-3">
				<input type="text" class="form-control" id="seach_partner" placeholder="Partner name">
			</div>

			<div class="col-md-2">
				<select class="form-control" id="search_status">
		            <option value=>Status</option>
		            @if( @request()->input('log_type') != 'error' )
		            <option value="200">200</option>
		            @endif
		            <option value="401">401</option>
		            <option value="404">404</option>
		        </select>
			</div>

			<div class="col-md-2">
				<input type="text" class="form-control" id="search_date_start" placeholder="Date time start">
			</div>

			<div class="col-md-2">
				<input type="text" class="form-control" id="search_date_end" placeholder="Date time end">
			</div>

		</div>

		<div class="table-responsive">
			{!! $dataTable->table(['id'=> 'tb_main', 'class'=> 'table table-bordered table-hover wid100']) !!}
		</div>

	</div>
</div>

@endsection


@push('add_js')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
{!! $dataTable->scripts() !!}

<script src="{{ asset('plugins/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bootstrap4-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>

$(document).ready(function(){

// $('#search_date_start').datetimepicker({
// 	format: 'YYYY-MM-DD'
// });

let search_name = "<?php echo Request::input('search_name');?>";
setTimeout(() => {
	if( search_name ){
		$('#seach_name').val(search_name).keyup();
	}
	
}, 500);

});


$("#export_logs").submit(function(){

	$("#export_logs input[name=seach_name]").val( $("#seach_name").val() );
	$("#export_logs input[name=seach_partner]").val( $("#seach_partner").val() );
	$("#export_logs input[name=search_status]").val( $("#search_status").val() );
	$("#export_logs input[name=search_date_start]").val( $("#search_date_start").val() );
	$("#export_logs input[name=search_date_end]").val( $("#search_date_end").val() );
	// return false;
});


</script>
@endpush
