
@extends("backend/layouts/backend")


@section('content')
<?php
// function prettyPrint( $json )
// {
//     $result = '';
//     $level = 0;
//     $in_quotes = false;
//     $in_escape = false;
//     $ends_line_level = NULL;
//     $json_length = strlen( $json );

//     for( $i = 0; $i < $json_length; $i++ ) {
//         $char = $json[$i];
//         $new_line_level = NULL;
//         $post = "";
//         if( $ends_line_level !== NULL ) {
//             $new_line_level = $ends_line_level;
//             $ends_line_level = NULL;
//         }
//         if ( $in_escape ) {
//             $in_escape = false;
//         } else if( $char === '"' ) {
//             $in_quotes = !$in_quotes;
//         } else if( ! $in_quotes ) {
//             switch( $char ) {
//                 case '}': case ']':
//                     $level--;
//                     $ends_line_level = NULL;
//                     $new_line_level = $level;
//                     break;

//                 case '{': case '[':
//                     $level++;
//                 case ',':
//                     $ends_line_level = $level;
//                     break;

//                 case ':':
//                     $post = " ";
//                     break;

//                 case " ": case "\t": case "\n": case "\r":
//                     $char = "";
//                     $ends_line_level = $new_line_level;
//                     $new_line_level = NULL;
//                     break;
//             }
//         } else if ( $char === '\\' ) {
//             $in_escape = true;
//         }
//         if( $new_line_level !== NULL ) {
//             $result .= "\n".str_repeat( "\t", $new_line_level );
//         }
//         $result .= $char.$post;
//     }

//     return $result;
// }


// $pos = strpos($log->txt_data, '[');
// $sub_str = substr_replace($log->txt_data, "\n", $pos, $pos);
// echo $sub_str;
?>
<div class="card">
	<div class="card-body">
		<h2>API Log Detail</h2>
		
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<tbody>
					<tr>
						<th style="width:200px;">Code</th>
						<td>{{ $log->status_code }}</td>
					</tr>
					<tr>
						<th>ip</th>
						<td>{{ $log->ip }}</td>
					</tr>

					<?php
					if( $log->log_type == 'req' )
						$type = 'Require';
					else
						$type = 'Request';
					?>
					<tr>
						<th>Type</th>
						<td>{{ $type }}</td>
					</tr>
					<tr>
						<th>Schema</th>
						<td>{{ $log->path }}</td>
					</tr>
					<tr>
						<th>API Name</th>
						<td>{{ $log->api_name ? $log->api_name : '-' }}</td>
					</tr>
					<tr>
						<th>Agent</th>
						<td>{{ $log->agent }}</td>
					</tr>
					<tr>
						<th>Partner Name</th>
						<td>{{ $log->partner_name ? $log->partner_name : '-' }}</td>
					</tr>

					<?php
					$c = new \Carbon\Carbon;
					$parse = $c->parse($log->datetime_at);
					?>
					<tr>
						<th>Date Time</th>
						<td>{{ $parse->format('d/m/Y H:i:s') }}</td>
					</tr>
					<tr>
						<th>Data</th>
						<td>
							<?php echo htmlentities($log->txt_data);?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<a href="{{ route('backend.apilog.index') }}" class="btn btn-secondary btn_form">Back</a>

	</div>
</div>
@endsection

@push('add_js')
<script>



</script>
@endpush
