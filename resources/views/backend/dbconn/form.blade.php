

@push('add_css')
<style>

</style>
@endpush

<div id="ajax_validate"></div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Database type : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<select class="form-control" name="conn_type" id="conn_type">
    		<option value="mysql" {{ @$conn->conn_type=='mysql'? 'selected':'' }}>MySQL</option>
    		<option value="sqlsrv" {{ @$conn->conn_type=='sqlsrv'? 'selected':'' }}>MSSQL</option>
    		<option value="oracle" {{ @$conn->conn_type=='oracle'? 'selected':'' }}>Oracle</option>
    	</select>
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Database connection name : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" class="form-control" name="conn_name" value="{{ @$conn->conn_name }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Alias : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" class="form-control alias_str" name="conn_alias" value="{{ @$conn->conn_alias }}" {{ @$conn->id? 'disabled':'' }}>
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">IP Address : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" class="form-control" name="conn_ip" id="conn_ip" value="{{ @$conn->conn_ip }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Username : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" class="form-control" name="username" id="conn_username" value="{{ @$conn->username }}">
    </div>
</div>

<?php
$pass = '';
if( @$conn->password )
	$pass = decrypt($conn->password);
?>
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Password : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="password" class="form-control" name="password" id="conn_password" value="{{ $pass }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Port : </label>
	<div class="col-sm-10">
    	<input type="text" class="form-control num_only" name="conn_port" id="conn_port" value="{{ @$conn->conn_port }}">
    </div>
</div>

<div class="form-group row" id="group_sid">
	<label class="col-sm-2 col-form-label">Service Name / SID : </label>
	<div class="col-sm-10">
    	<input type="text" class="form-control" name="conn_sid" id="sid" value="{{ @$conn->conn_sid }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label"></label>
	<div class="col-sm-10">
    	<button type="button" class="btn btn-primary btn_test_connect">Test Connection</button>
    	<button type="button" class="btn btn-primary btn_connect_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
    	&nbsp;
    	<span class="conenct_status"></span>
    </div>
</div>


<div class="form-group row" id="group_dbconn">
	<label class="col-sm-2 col-form-label">Database name :</label>
	<div class="col-sm-10">
    	<select class="form-control" name="conn_database" id="conn_database">
    		<option value>-- Select Database --</option>
    	</select>
    </div>
</div>


<div class="form-group row label_status">
	<label class="col-sm-2 col-form-label">Status : </label>
	<div class="col-sm-10">
		<?php
		$checked = 1;
		if( isset($conn->conn_status) && $conn->conn_status == 0 )
			$checked = 0;
		?>
    	<label>
    		<input type="radio" name="conn_status" value="1" {{ $checked==1? 'checked':'' }}>
    		Enable
    	</label>
    	&nbsp;
    	<label>
    		<input type="radio" name="conn_status" value="0" {{ $checked==0? 'checked':'' }}>
    		Disabled
    	</label>
    </div>
</div>


@push('add_js')
<script>
let connected = 0;
$(document).ready(function(){

set_group_dbconn();

$('#conn_type').change(function(){
	reset_connect();
	set_group_dbconn();
});
$('#conn_ip, #conn_username, #conn_password, #conn_port, #sid').keyup(function(){
	reset_connect();
});

function reset_connect()
{
	$('.conenct_status').html('');
	$('#conn_database').html('<option value>-- Select Database --</option>');
	connected = 0;
}
function set_group_dbconn()
{
	if( $('#conn_type').val() == 'oracle' ){
		$('#group_dbconn').hide();
		$('#group_sid').show();
	}else{
		$('#group_dbconn').show();
		$('#group_sid').hide();
	}
}


@if( @$conn->id != '' )
	$('.btn_test_connect').hide();
	$('.btn_connect_loading').show();
	
	$.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		type: 'post',
		url: "{{ route('backend.dbconn.test_connect') }}",
		data: {
			conn_type: $("select[name=conn_type]").val(),
			conn_ip: $("input[name=conn_ip]").val(),
			username: $("input[name=username]").val(),
			password: $("input[name=password]").val(),
			conn_port: $("input[name=conn_port]").val(),
			conn_sid: $("input[name=conn_sid]").val()
		},
		// processData: false,
		// contentType: false,
		success: function(res)
		{
			console.log(res);
			$('.btn_test_connect').show();
			$('.btn_connect_loading').hide();

			if( res.status == 'success' ){
				let html = '';
				$.each(res.data, function(index, val){
					let selected = '';
					if( val.name == "{{ $conn->conn_database }}" )
						selected = 'selected';
					html += "<option value='"+val.name+"' "+selected+">"+val.name+"</option>";
				});
				
				$('#conn_database').html( html );
				$('.conenct_status').html("<span class='text-success'>Connect to "+$("input[name=conn_ip]").val()+" completes !</span>");
				connected = 1;

			}else{
				$('.conenct_status').html("<span class='text-danger'>"+res.msg+"</span>");
				connected = 0;
			}
		},
	    error: function (jqXHR, exception)
	    {
	    	$('.btn_test_connect').show();
			$('.btn_connect_loading').hide();
	        let msg = set_ajax_error(jqXHR, exception);
	        swal({
				type: 'error',
				title: 'Error',
				text: msg,
			});
	    },
	});
@endif


$(".btn_test_connect").click(function(){
	

	if( $("select[name=conn_type]").val().trim() == '' || $("input[name=conn_ip]").val().trim() == '' || $("input[name=username]").val().trim() == '' ){
		swal({
			type: 'warning',
			title: 'Please fill in the information.',
			text: 'Please enter Database type, IP Address, Username for test connection.',
		});
		return false;
	}else if( $('#conn_type').val() == 'oracle' && $("#sid").val().trim() == '' ){
		swal({
			type: 'warning',
			title: 'Please fill Service Name / SID.',
			text: 'Please enter Service Name / SID if you connect type Oracle.',
		});
		return false;
	}

	$('.btn_test_connect').hide();
	$('.btn_connect_loading').show();

	$.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		type: 'post',
		url: "{{ route('backend.dbconn.test_connect') }}",
		data: {
			conn_type: $("select[name=conn_type]").val(),
			conn_ip: $("input[name=conn_ip]").val(),
			username: $("input[name=username]").val(),
			password: $("input[name=password]").val(),
			conn_port: $("input[name=conn_port]").val(),
			conn_sid: $("input[name=conn_sid]").val()
		},
		// processData: false,
		// contentType: false,
		success: function(res)
		{
			console.log(res);
			$('.btn_test_connect').show();
			$('.btn_connect_loading').hide();

			if( res.status == 'success' ){
				let html = '';
				$.each(res.data, function(index, val){
					html += "<option value='"+val.name+"'>"+val.name+"</option>";
				});
				
				$('#conn_database').html( html );
				$('.conenct_status').html("<span class='text-success'>Connecte to "+$("input[name=conn_ip]").val()+" complete !</span>");
				connected = 1;

			}else{
				$('.conenct_status').html("<span class='text-danger'>"+res.msg+"</span>");
				connected = 0;
			}
		},
	    error: function (jqXHR, exception)
	    {
	    	$('.btn_test_connect').show();
			$('.btn_connect_loading').hide();
	        let msg = set_ajax_error(jqXHR, exception);
	        swal({
				type: 'error',
				title: 'Error',
				text: msg,
			});
	    },
	});
});


// Submit Form
<?php
if( @$conn->id != '' )
	$url = route('backend.dbconn.update', $conn->id);
else
	$url = route('backend.dbconn.store');
?>
$('#form_db_connect').submit(function(){

	if( connected == 0 ){
		swal({
			type: 'warning',
			title: 'Please test connect before submit.',
		});
		return false;
	}

	$('.btn_dbconnect_default').hide();
	$('.btn_dbconnect_loading').show();

	var formData = new FormData($("#form_db_connect")[0]);
	@if( @$conn->id != '' )
	formData.append('_method', 'PUT');
	@endif
	
	$.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		url: "{{ $url }}",
		type: "post",
		data: formData,
		processData: false,
		contentType: false,
		success: function(res)
		{
			console.log(res);
			$('.btn_dbconnect_default').show();
			$('.btn_dbconnect_loading').hide();

			if( res.status == 'success' ){
				swal({
					type: "success",
					title: 'Success',
					text: 'Database connection Created.',
				})
				.then(function(res){
					@if( @$conn->id != '' )
					window.location.reload();
					@else
					window.location.href = "{{ route('backend.dbconn.index') }}";
					@endif
				});

			}else{
				var html = html_alert(res.data);
				$('#ajax_validate').html(html);
			}

		},
		error: function (jqXHR, exception)
	    {
	    	$('.btn_dbconnect_default').show();
			$('.btn_dbconnect_loading').hide();
	        let msg = set_ajax_error(jqXHR, exception);
	        swal({
				type: 'error',
				title: 'Error',
				text: msg,
			});
	    },
	});

	return false;
});

});



</script>
@endpush