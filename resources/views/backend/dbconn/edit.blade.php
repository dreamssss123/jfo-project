
@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Edit Database connection</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<form method="post" action="javascript:" id="form_db_connect">
			@csrf
			
			@include('backend.dbconn.form')

			<div class="text-right">
				<a href="{{ route('backend.dbconn.index') }}" class="btn btn-secondary btn_form">Cancel</a>
				&nbsp;
				<button type="submit" class="btn btn_pink btn_form btn_dbconnect_default">Save</button>
				<button type="submit" class="btn btn_pink btn_form btn_dbconnect_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
			</div>
		</form>

	</div>
</div>
@endsection