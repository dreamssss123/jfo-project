
@extends("backend/layouts/backend")


@push('add_css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}">
<style>

</style>
@endpush

@section('content')
<?php
$perms = \App\Perm::can();
?>
<div class="section-header">
	<h2>
		Database connection
		<div class="pull-right">
			@if( $perms[4]->create )
			<a href="{{ route('backend.dbconn.create') }}" class="btn btn-sm btn_pink"><i class="fa fa-plus"></i>&nbsp; Create connection</a>
			@endif
		</div>
	</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<div class="table-responsive">
			{!! $dataTable->table(['id'=> 'tb_main', 'class'=> 'table table-bordered table-hover wid100']) !!}
		</div>

	</div>
</div>

@endsection


@push('add_js')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('../vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side.js') }}"></script> -->
{!! $dataTable->scripts() !!}
@endpush
