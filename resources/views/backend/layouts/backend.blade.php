
@extends("layouts/frame")


@section('frame')

<?php
$user = Auth::user();
?>
<div class="wrapper boxed-wrapper">
  <header class="main-header"> 
    <a href="{{ route('backend.index') }}" class="logo blue-bg"> 
        <!-- <span class="logo-mini">
            <img src="{{ asset('images/no_img.jpg') }}" alt="">
        </span> 
        
        <span class="logo-lg">
            <img src="{{ asset('images/no_img.jpg') }}" alt="">
        </span> -->
        JFO API Gateway
    </a> 
    
    <nav class="navbar blue-bg navbar-static-top"> 
      
      <ul class="nav navbar-nav pull-left">
        <li><a class="sidebar-toggle" data-toggle="push-menu" href=""></a> </li>
      </ul>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i></a>
            <ul class="dropdown-menu">
              <li class="header">ล่าสุด</li>
              <li>
                <ul class="menu">
                    <li>
	                    <a href="">
	                      <div class="pull-left">
	                        <img src="{{ asset('images/no_img.jpg') }}" class="img-circle" alt="User Image">
	                      </div>
	                      <h4>ชื่อทดสอบ</h4>
	                      <p>ผู้สมัครได้ส่งคำร้องสมัครสมาชิก</p>
	                      <p><span class="time">10/09/2019 00:22:55</span></p>
	                    </a>
 					          </li>
                </ul>
              </li>
              <li class="footer"><a href="">View All Messages</a></li>
            </ul>
          </li> -->

          <li class="dropdown user user-menu p-ph-res"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ asset('images/no_img.jpg') }}" class="user-image" alt="User Image"> <span class="hidden-xs">{{ $user->firstname }}</span> </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <div class="pull-left user-img"><img src="{{ asset('images/no_img.jpg') }}" class="img-responsive" alt="User"></div>
                <p class="text-left">{{ $user->firstname }} <small>{{ $user->email }}</small> </p>
                <!-- <div class="view-link text-left"><a href="#">View Profile</a> </div> -->
              </li>
              <!-- <li><a href="#"><i class="icon-profile-male"></i> My Profile</a></li> -->
              <!-- <li><a href="#"><i class="icon-wallet"></i> My Balance</a></li> -->
              <!-- <li><a href="#"><i class="icon-envelope"></i> Inbox</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#"><i class="icon-gears"></i> Account Setting</a></li> -->
              <li role="separator" class="divider"></li>
              <li><a href="{{ route('backend.auth.logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <div class="sidebar"> 
      <!-- Sidebar user panel -->
      <!-- {{-- <div class="user-panel">
        <div class="image text-center"><img src="{{ asset('images/no_img.jpg') }}" class="img-circle" alt="User Image"> </div>
        <div class="info">
          <p>OFza123</p>
          <a href="{{ asset('') }}"><i class="fa fa-home"></i></a>
          <a href=""><i class="fa fa-power-off"></i></a>
      	</div>
      </div> --}} -->
      
      <?php
      $perms = \App\Perm::can();
      ?>
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="header">Menu</li> -->

        @if( @$perms[1]->view )
        <li><a href="{{ route('backend.index') }}"><i class="fa fa-tachometer"></i> Dashboard</a> </li>
        @endif

        @if( @$perms[2]->view || @$perms[11]->view  )
        <?php
        $active = '';
        if( Request::segment(2) == 'user' || Request::segment(2) == 'role' )
            $active = 'active';
        ?>
        <li class="treeview {{ $active }}"> <a href="#"> <i class="fa fa-user"></i> <span>User</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
            <ul class="treeview-menu">
                @if( @$perms[2]->view )
                <li class="{{ Request::segment(2)=='user'?'active':'' }}"><a href="{{ route('backend.user.index') }}">User</a></li>
                @endif
                @if( @$perms[11]->view )
                <li class="{{ Request::segment(2)=='role'?'active':'' }}"><a href="{{ route('backend.role.index') }}">Roles & Permission</a></li>
                @endif
            </ul>
        </li>
        @endif

        
        @if( @$perms[9]->view || @$perms[3]->view  )
        <?php
        $active = '';
        if( Request::segment(2) == 'partner_org' || Request::segment(2) == 'partner' )
            $active = 'active';
        ?>
        <li class="treeview {{ $active }}"> <a href="#"> <i class="fa fa-user-circle-o"></i> <span>Partner</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
            <ul class="treeview-menu">
              @if( @$perms[9]->view )
              <li class="{{ Request::segment(2)=='partner_org'?'active':'' }}"><a href="{{ route('backend.partner_org.index') }}">Partner Organization</a></li>
              @endif
              @if( @$perms[3]->view )
              <li class="{{ Request::segment(2)=='partner'?'active':'' }}"><a href="{{ route('backend.partner.index') }}">Partner</a></li>
              @endif
            </ul>
        </li>
        @endif


        @if( @$perms[4]->view )
        <li><a href="{{ route('backend.dbconn.index') }}"><i class="fa fa-database"></i> Database Connection</a> </li>
        @endif

        @if( @$perms[5]->view )
        <li><a href="{{ route('backend.rest.index') }}"><i class="fa fa-plane"></i> API</a> </li>
        @endif

        @if( @$perms[7]->view )
        <?php
        $active = '';
        if( Request::segment(2) == 'apilog' )
            $active = 'active';
        ?>
        <li class="treeview {{ $active }}"> <a href="#"> <i class="fa fa-table"></i> <span>Log</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
            <ul class="treeview-menu">
                <li class="{{ request::segment(2)=='apilog'&&@request()->input('log_type')!='error'?'active':'' }}"><a href="{{ route('backend.apilog.index') }}">API Log</a></li>
                <li class="{{ request::segment(2)=='apilog'&&@request()->input('log_type')=='error'?'active':'' }}"><a href="{{ route('backend.apilog.index') }}?log_type=error">API Error Log</a></li>
            </ul>
        </li>
        @endif

        @if( @$perms[10]->view )
        <li><a href="{{ route('backend.systemlog.index') }}"><i class="fa fa-table"></i> System Log</a> </li>
        @endif

        @if( @$perms[6]->view )
        <li><a href="{{ route('backend.report.index') }}"><i class="fa fa-table"></i> Report</a> </li>
        @endif

        @if( @$perms[8]->view )
        <li><a href="{{ route('backend.setting.index') }}"><i class="fa fa-cog"></i> Setting</a> </li>
        @endif

        

        
        <!-- <li class="treeview"> <a href="#"> <i class="fa fa-cog"></i> <span>ตั้งค่า</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
          	<ul class="treeview-menu">
	            <li><a href="{{ route('backend.role.index') }}">Roles & Permission</a></li>
          	</ul>
	       </li> -->
        

        
      </ul>

    </div>
    <!-- /.sidebar --> 
  </aside>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 

    <!-- Main content -->
    <div class="content"> 
        
        @yield('content')
		
    </div>
    <!-- /.content --> 
  </div>

  <!-- /.content-wrapper -->
  <!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">Version 1.2</div>
    Copyright © 2017 API-Project. All rights reserved.
  </footer> -->
</div>

@endsection