
@extends("backend/layouts/backend")

@push('add_css')
<style>
.db_btn a{height:100%; color:#666f73; font-size:20px;}
.db_btn a:hover{background-color:#ccc;}
table.table{margin-bottom:0;}
</style>
@endpush

@section('content')
<?php
$c = new Carbon\Carbon;
?>
<div class="row">
	
	<div class="col-md-12">
		@include('layouts.errors')
	</div>

	<div class="col-md-6">
		
		<div class="card" style="height:425px;">
			<div class="card-body">
				<h2>Most concurrent api / Day</h2>
				
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>API name</th>
								<th style="text-align:center;">Quantity</th>
								<th style="text-align:center;">View</th>
							</tr>
						</thead>

						<tbody>
							@if( count($log_pops) > 0  )
								@foreach( $log_pops as $row )
								<?php
								$keyword = $row->api_name;
								if( $row->path == 'auth/login' )
									$keyword = 'auth_login';
								?>
								<tr>
									<td>api/{{ $row->path }}</td>
									<td style="text-align:center;">{{ $row->total }}</td>
									<td style="text-align:center;">
										<a href="{{ route('backend.report.index').'?search_name='.$keyword }}" class="btn btn-info btn-sm">more</a>
									</td>
								</tr>
								@endforeach
							@else
							<tr>
								<td colspan="3" style="text-align:center;">No data !</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>

	<div class="col-md-6">
		
		<div class="card" style="height:425px;">
			<div class="card-body">
				<h2>Error log lastest</h2>
				
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>API name</th>
								<th>Datetime at</th>
								<th style="text-align:center;">View</th>
							</tr>
						</thead>

						<tbody>
							@if( count($log_errors) > 0  )
								@foreach( $log_errors as $row )
								<tr>
									<td>api/{{ $row->path }}</td>
									<?php
									$p = $c->parse($row->datetime_at);
									echo "<td>".$p->format('d/m/Y H:i')."</td>";
									?>
									<td style="text-align:center;">
										<a href="{{ route('backend.apilog.index').'?log_type=error&search_name='.$row->path }}" class="btn btn-info btn-sm">more</a>
									</td>
								</tr>
								@endforeach
							@else
							<tr>
								<td colspan="3" style="text-align:center;">No data !</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row mt-3 db_btn">
	<div class="col-md-3">
		<a href="{{ route('backend.partner.index') }}" class="card text-center">
			<div class="card-body">
				<span>Partner</span>
				<span>Management</span>
			</div>
		</a>
	</div>
		

	<div class="col-md-3">
		<a href="{{ route('backend.dbconn.index') }}" class="card text-center">
			<div class="card-body">
				<span>Database Connection</span>
				<!-- <span>Management</span> -->
			</div>
		</a>
	</div>

	<div class="col-md-3">
		<a href="{{ route('backend.rest.index') }}" class="card text-center">
			<div class="card-body">
				<span>API Schema</span>
			</div>
		</a>
	</div>

	<div class="col-md-3">
		<a href="{{ route('backend.report.index') }}" class="card text-center">
			<div class="card-body">
				<span>Report</span>
			</div>
		</a>
	</div>
</div>

<div class="row mt-3">
	<div class="col-md-12">
		
		<div class="card">
			<div class="card-body">
				<h2>
					<!-- Most concerrent api / Day -->
					API
					<div class="pull-right">
						<a href="{{ route('backend.rest.index') }}" class="btn btn-info btn-sm">ดูทั้งหมด</a>
					</div>
				</h2>
				
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>API name</th>
								<th>Route name</th>
								<th style="text-align:center;">Usage</th>
							</tr>
						</thead>

						<tbody>
							<?php
							if( count($report) > 0 ){
								$run = 1;
								foreach( $report as $row ){

									$name = $row->api_name;
									if( !$name )
										$name = '-';

									echo "<tr>";
									echo 	"<td>".$run."</td>";
									echo 	"<td>".$name."</td>";
									echo 	"<td>api/".$row->path."</td>";
									echo 	"<td style='text-align:center;'>".$row->total."</td>";
									echo "</tr>";

									$run++;
								}
							}else{
								echo "<tr> <td colspan=4 align=center>No Data !</td> </tr>";
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('add_js')
<script>

// $.ajax({
// 	// headers: {
// 	//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
// 	//     },
// 	type: 'post',
// 	url: "http://localhost/small_work/api_project/public/api/test_content",
// 	success: (res) => {
// 		console.log(res);
// 	}
// });

</script>
@endpush
