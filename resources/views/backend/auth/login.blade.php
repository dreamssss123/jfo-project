@extends("layouts/frame")


@push('add_css')
<style>
body{background-image:url("{{ asset('images/body-bg2.jpg') }}"); background-position:center; -webkit-background-size:cover; -moz-background-size:cover; background-size:cover; -o-background-size:cover;}
.btn_login:hover{opacity:0.8;}
</style>
@endpush

@section('frame')

<div style="margin-top:5%; text-align:center;">
  <a><img src="{{ asset('images/logo2.png') }}" style="width:300px;"></a>
  <h1 style="color:#fff;">ระบบบริหารการเชื่อมโยงข้อมูล</h1>
</div>

<div class="login-box" style="display:block; margin-top:3%; margin-bottom:3%; text-align:center;">

  <div class="login-box-body">
    <h2 class="login-box-msg mb-4" style="color:#0c4da2;">Sign In</h2>
    <!-- <p class="text-center h5">to</p>
    <h4 class="text-center">JFO API Gateway</h4> -->

    @include('layouts.errors')
    
    <form method="post" action="{{ route('backend.auth.login_submit') }}">
      @csrf
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control sty1" placeholder="User">
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control sty1" placeholder="Password">
      </div>
      <div>
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!-- <label>
              <input type="checkbox">
              Remember Me </label>
            <a href="pages-recover-password.html" class="pull-right"><i class="fa fa-lock"></i> Forgot pwd?</a> --> 
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4 m-t-4">
         <!--  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button> -->
          <button type="submit" class="btn_login" style="border:none; background-color:transparent; cursor:pointer;">
            <img src="{{ asset('images/btn_login.jpg') }}" style="max-width:100%;">
          </button>
        </div>
        <!-- /.col --> 
      </div>
    </form>
    <!-- /.social-auth-links -->
    
    <!-- <div class="m-t-2">Don't have an account? <a href="pages-register.html" class="text-center">Sign Up</a></div> -->
  </div>
  <!-- /.login-box-body --> 
</div>
<!-- /.login-box -->

<p style="text-align:center; color:#fff; font-size:18px;">สงวนสิทธิ์ © 2563 สำนักงานกองทุนยุติธรรม สำนักงานปลัดกระทรวงยุติธรรม</p>

@endsection