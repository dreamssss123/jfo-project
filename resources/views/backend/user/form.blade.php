
<div class="form-group row">
	<label class="col-sm-2 col-form-label">First Name : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" name="firstname" class="form-control" value="{{ old('firstname')?old('firstname'):@$user->firstname }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Last Name :</label>
	<div class="col-sm-10">
    	<input type="text" name="lastname" class="form-control" value="{{ old('lastname')?old('lastname'):@$user->lastname }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Email : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="email" name="email" class="form-control" value="{{ old('email')?old('email'):@$user->email }}" {{ @$user->id?'disabled':'' }}>
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Password : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="password" name="password" class="form-control alias_str" value="{{ @$user->id? $user->blank_password:'' }}">
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-10">
    	<span class="req">must be at least 6 characters long, characters a-z, A-Z, 0-9, Symbols</span>
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Confirm Password : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="password" name="password_confirmation" class="form-control alias_str">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Organization :</label>
	<div class="col-sm-10">
    	<input type="text" name="organization" class="form-control" value="{{ old('organization')?old('organization'):@$user->organization }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Position :</label>
	<div class="col-sm-10">
    	<input type="text" name="position" class="form-control" value="{{ old('position')?old('position'):@$user->position }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Mobile/Tel :</label>
	<div class="col-sm-10">
    	<input type="text" name="phone" class="form-control" value="{{ old('phone')?old('phone'):@$user->phone }}">
    </div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Address :</label>
	<div class="col-sm-10">
    	<textarea class="form-control" name="address">{{ old('address')?old('address'):@$user->address }}</textarea>
    </div>
</div>

<?php
$roles = \App\Role::all();
$select_role = old('role_id')?old('role_id'):@$user->role_id;
?>
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Role : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<select class="form-control" name="role_id">
    		<option value>Select Role</option>
            @foreach( $roles as $role )
            <option value="{{ $role->id }}" {{ $select_role==$role->id? 'selected':'' }}>{{ $role->name }}</option>
            @endforeach
    	</select>
    </div>
</div>

<?php
$select_role = old('status')?old('status'):@$user->status;
?>
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Status :</label>
	<div class="col-sm-10">
    	<label>
    		<input type="radio" name="status" value="1" {{ $select_role==1? 'checked':'' }}>
    		Enable
    	</label>
		&nbsp; &nbsp; &nbsp;
    	<label>
    		<input type="radio" name="status" value="0" {{ $select_role==0? 'checked':'' }}>
    		disable
    	</label>
    </div>
</div>