

@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Edit User</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<form method="post" action="{{ route('backend.user.update', $user->id) }}">
			@csrf
			{{ method_field('PUT') }}
			
			@include('backend.user.form')

			<div class="text-right">
				<a href="{{ route('backend.user.index') }}" class="btn btn-secondary btn_form">Cancel</a>
				&nbsp;
				<button type="submit" class="btn btn_pink btn_form">Save</button>
			</div>
		</form>

	</div>
</div>
@endsection