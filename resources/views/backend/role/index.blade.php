
@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')
<?php
$perms = \App\Perm::can();
// print_r($perms);
?>

<div class="section-header">
	<h2>
		Role & Permission
		<div class="pull-right">
			@if( $perms[11]->create )
			<a href="{{ route('backend.role.create') }}" class="btn btn-sm btn_pink"><i class="fa fa-plus"></i>&nbsp; Create Role</a>
			@endif
		</div>
	</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<div class="table-responsive">
			<table id="tb_main" class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Role name</th>
						<th>Status</th>
						<th>Modified date</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					@if( count($roles) > 0 )
						<?php
						$run = 1;
						foreach( $roles as $role ){

							$parse = \Carbon\Carbon::parse($role->updated_at);

							if( $role->status == 1 )
								$status = "<span class='stat_success'>enable</span>";
							else
								$status = "<span class='stat_danger'>disable</span>";

							echo "<tr>";
							echo 	"<td>".$run."</td>";
							echo 	"<td>".$role->name."</td>";
							echo 	"<td>".$status."</td>";
							echo 	"<td>".$parse->format('d/m/Y H:i')."</td>";
							echo 	"<td>";

							if( $perms[11]->edit )
								echo 	"<a href='".route('backend.role.edit', $role->id)."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a>&nbsp;";

							if( $perms[11]->delete ){
								if( $role->id != config('app.sa_id') ){
									echo 		"<form method='post' action='".route('backend.role.destroy', $role->id)."' style='display:inline-block'>";
									echo 			csrf_field();
									echo 			"<input type='hidden' name='_method' value='DELETE'>";
		                			echo 			"<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
									echo 		"</form>";
								}
							}

							echo 	"</td>";
							echo "</tr>";

							$run++;
						}
						?>
					@else
						<tr>
							<td colspan=4 align=center>No Data</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>

	</div>
</div>

@endsection


@push('add_js')

@endpush
