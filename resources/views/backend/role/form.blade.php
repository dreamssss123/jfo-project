

@push('add_css')
<style>
.label_status label{margin:0;}
#tb_main th,
#tb_main td{text-align:center;}
#tb_main tr th:first-child,
#tb_main tr td:first-child{text-align:left;}
</style>
@endpush

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Role Name : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" name="name" class="form-control" value="{{ @$role->name }}" {{ @$role->id? 'disabled':'' }}>
    </div>
</div>


<div class="form-group row">
	<label class="col-sm-2 col-form-label">Permission : <span class="req">*</span></label>
	<div class="col-sm-10">
		<div class="table-responsive">
			<table id="tb_main" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Menu</th>
						<th><i class="fa fa-eye"></i> View</th>
						<th><i class="fa fa-plus"></i> Add</th>
						<th><i class="fa fa-edit"></i> Edit</th>
						<th><i class="fa fa-trash"></i> Delete</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				$menus = \App\Menu::orderBy('sort', 'asc')->get();

				$p = new \App\Perm;
				foreach( $menus as $menu ){

					$can_view = '';
					$can_create = '';
					$can_edit = '';
					$can_delete = '';

					if( @$role->id ){
						$can = $p->where('role_id', $role->id)
								->where('menu_id', $menu->id)
								->first();

						if( @$can->can_view == 1 ) $can_view = 'checked';
						if( @$can->can_create == 1 ) $can_create = 'checked';
						if( @$can->can_edit == 1 ) $can_edit = 'checked';
						if( @$can->can_delete == 1 ) $can_delete = 'checked';
					}
					

					echo "<tr>";
					echo 	"<td>".$menu->name."</td>";
					echo 	"<td><input type='checkbox' name='can_view[".$menu->id."]' ".$can_view."></td>";
					echo 	"<td><input type='checkbox' name='can_create[".$menu->id."]' ".$can_create."></td>";
					echo 	"<td><input type='checkbox' name='can_edit[".$menu->id."]' ".$can_edit."></td>";
					echo 	"<td><input type='checkbox' name='can_delete[".$menu->id."]' ".$can_delete."></td>";
					echo "</tr>";
				}
				?>

				@if( !@$role->id )
					<?php
					// $p = new \App\Perm;
					// foreach( $menus as $menu ){

						// $can_view
					?>
					<!-- <tr>
						<td>{{ $menu->name }}</td>
						<td>
							<input type="checkbox" name="can_view[{{ $menu->id }}]">
						</td>
						<td>
							<input type="checkbox" name="can_create[{{ $menu->id }}]">
						</td>
						<td>
							<input type="checkbox" name="can_edit[{{ $menu->id }}]">
						</td>
						<td>
							<input type="checkbox" name="can_delete[{{ $menu->id }}]">
						</td>
					</tr> -->
					<?php
					// }
					?>
				@else

					<?php
					foreach( $role_perm as $perm ){
					?>
						<!-- <tr>
							<td>{{ $perm->menu->name }}</td>
							<td>
								<input type="checkbox" name="can_view[{{ $perm->id }}]" {{ $perm->can_view==1?'checked':'' }}>
							</td>
							<td>
								<input type="checkbox" name="can_create[{{ $perm->id }}]" {{ $perm->can_create==1?'checked':'' }}>
							</td>
							<td>
								<input type="checkbox" name="can_edit[{{ $perm->id }}]" {{ $perm->can_edit==1?'checked':'' }}>
							</td>
							<td>
								<input type="checkbox" name="can_delete[{{ $perm->id }}]" {{ $perm->can_delete==1?'checked':'' }}>
							</td>
						</tr> -->
					<?php
					}
					?>
				@endif
				</tbody>


			</table>
		</div>
	</div>
</div>



<!-- Super Admin Cannot Disable -->

@if( @$role->id != config('app.sa_id') )
<div class="form-group row label_status">
	<label class="col-sm-2 col-form-label">Status : </label>
	<div class="col-sm-10">
		<?php
		$checked = 1;
		if( isset($role->status) && $role->status == 0 )
			$checked = 0;
		?>
    	<label>
    		<input type="radio" name="status" value="1" {{ $checked==1? 'checked':'' }}>
    		Enable
    	</label>
    	&nbsp;
    	<label>
    		<input type="radio" name="status" value="0" {{ $checked==0? 'checked':'' }}>
    		Disabled
    	</label>
    </div>
</div>
@else
<input type="radio" name="status" value="1" style="display:none !important;" checked>
@endif

