
@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Create Role & Permission</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<form method="post" action="{{ route('backend.role.store') }}">
			@csrf
			
			@include('backend.role.form')

			<div class="text-right">
				<a href="{{ route('backend.role.index') }}" class="btn btn-secondary btn_form">Cancel</a>
				&nbsp;
				<button type="submit" class="btn btn_pink btn_form">Save</button>
			</div>
		</form>

	</div>
</div>
@endsection