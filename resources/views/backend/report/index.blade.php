
@extends("backend/layouts/backend")


@push('add_css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bootstrap4-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css') }}">

<style>
.dataTables_filter{display: none;}
</style>
@endpush

@section('content')
<?php
$perms = \App\Perm::can();
$rq = new \Illuminate\Http\Request;
?>

<div class="section-header">
	<h2>
		Report
		<form method="post" action="{{ route('backend.report.export_data', 'csv') }}" id="export_logs" class="pull-right">
			@csrf
			<input type="hidden" name="search_name">
			<input type="hidden" name="search_partner">
			<input type="hidden" name="search_date_start">
			<input type="hidden" name="search_date_end">
			<button type="submit" class="btn btn_pink btn-sm" style="width:100px;">Export</button>
		</form>
	</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<div class="row">
			<div class="col-md-3">
				<input type="text" class="form-control" id="search_name" placeholder="Search API name">
			</div>

			<div class="col-md-3">
				<input type="text" class="form-control" id="search_partner" placeholder="Partner name">
			</div>

			<div class="col-md-2">
				<input type="text" class="form-control" id="search_date_start" placeholder="Date time start">
			</div>

			<div class="col-md-2">
				<input type="text" class="form-control" id="search_date_end" placeholder="Date time end">
			</div>
		</div>

		<div class="table-responsive">
			{!! $dataTable->table(['id'=> 'tb_main', 'class'=> 'table table-bordered table-hover wid100']) !!}
		</div>

	</div>
</div>
@endsection


@push('add_js')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('../vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side.js') }}"></script> -->
{!! $dataTable->scripts() !!}

<script src="{{ asset('plugins/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bootstrap4-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script>

$(document).ready(function(){


let search_name = "<?php echo Request::input('search_name');?>";
setTimeout(() => {
	if( search_name ){
		$('#search_name').val(search_name).keyup();
	}
	
}, 500);


$("#export_logs").submit(function(){

	$("#export_logs input[name=search_name]").val( $("#search_name").val() );
	$("#export_logs input[name=search_partner]").val( $("#search_partner").val() );
	$("#export_logs input[name=search_date_start]").val( $("#search_date_start").val() );
	$("#export_logs input[name=search_date_end]").val( $("#search_date_end").val() );
	// return false;
});

});

</script>
@endpush
