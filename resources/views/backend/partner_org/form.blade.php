

@push('add_css')
<style>

</style>
@endpush

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Partner organization : <span class="req">*</span></label>
	<div class="col-sm-10">
    	<input type="text" name="cat_name" class="form-control" value="{{ old('cat_name') ? old('cat_name'):@$cat->cat_name }}">
    </div>
</div>

<div class="form-group row label_status">
	<label class="col-sm-2 col-form-label">Status : </label>
	<div class="col-sm-10">
		<?php
		$checked = 1;
		if( isset($cat->status) && $cat->status == 0 )
			$checked = 0;
		?>
    	<label>
    		<input type="radio" name="status" value="1" {{ $checked==1? 'checked':'' }}>
    		Enable
    	</label>
    	&nbsp;
    	<label>
    		<input type="radio" name="status" value="0" {{ $checked==0? 'checked':'' }}>
    		Disabled
    	</label>
    </div>
</div>

