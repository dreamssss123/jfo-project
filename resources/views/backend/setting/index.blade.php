
@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Setting</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<form method="post" action="{{ route('backend.setting.store') }}" id="form_db_connect">
			@csrf

			<h3 class="mb-4">Email Notification</h3>
			
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Email admin : </label>
				<div class="col-sm-10">
			    	<input type="email" name="global_mail_error" class="form-control" value="{{ @$config->global_mail_error }}" required>
			    </div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Qty : </label>
				<div class="col-sm-10">
			    	<input type="int" name="global_quantity" class="form-control num_only" value="{{ @$config->global_quantity }}">
			    </div>
			</div>

			<div class="form-group row mb-5">
				<label class="col-sm-2 col-form-label">Time Alert : </label>
				<div class="col-sm-10">
			    	<input type="int" name="global_time_alert" class="form-control num_only" value="{{ @$config->global_time_alert }}" required>
			    </div>
			</div>


			<h3 class="mb-4">Page Limit</h3>
			
			<div class="form-group row mb-5">
				<label class="col-sm-2 col-form-label">Page Limit : </label>
				<div class="col-sm-10">
			    	<input type="text" name="global_page_limit" class="form-control num_only" value="{{ @$config->global_page_limit }}">
			    </div>
			</div>



			<div class="text-right">
				<button type="submit" class="btn btn_pink btn_form">Save</button>
			</div>
		</form>

	</div>
</div>
@endsection