
@extends("backend/layouts/backend")


@push('add_css')
<style>
#search_column{display:inline-block; width:300px;}
#condition{display:inline-block; width:150px;}
.pretty_code,
#pretty_json,
#pretty_xml{padding:10px; margin-bottom:15px; width:100%; max-height:200px; border:solid 1px #ccc;}

.box_tb{max-height:540px; overflow-y:scroll;}
#tb_step_3 tr th,
#tb_step_3 tr td{vertical-align:middle;}
#tb_step_3 tr td{padding-top:5px; padding-bottom:5px;}
#tb_step_3 tbody tr{cursor:pointer;}
#tb_step_3 tbody tr td:nth-child(1n) label{margin-bottom:0;}
#tb_step_3 tbody tr td:nth-child(1n) input[type=checkbox]{vertical-align:middle;}
</style>
@endpush

@section('content')

<?php

if( $rest->schema_req_markup ){
	$configs = json_decode($rest->schema_req_markup);
	// echo $rest->schema_req_markup;
	// print_r($configs);
}

?>
<div class="section-header">
	<h2>Create Schema</h2>
</div>

<div class="card noedge">
	
	@include('backend.rest.nav_step', ['step' => $step])

	<div class="card-body" id="form_step1">
		@include('layouts.errors')
		<div id="ajax_validate"></div>
	
		<!-- <pre id="qwe"><?php echo Response::json($rest, 200, array(), JSON_PRETTY_PRINT);?></pre> -->
		<div class="form-group row">
			<div class="col-sm-12">
		    	<input type="text" class="form-control" id="search_column">
		    	&nbsp;
		    	<span>Operator of all</span>
		    	&nbsp;
		    	<select class="form-control" id="condition">
		    		<option value="and" {{ @$configs->condition=='and'? 'selected':'' }}>AND</option>
		    		<option value="or" {{ @$configs->condition=='or'? 'selected':'' }}>OR</option>
		    	</select>
		    </div>
		</div>

		<div class="row">
			<div class="col-md-8 box_tb">
				<form method="post" action="javascript:" class="table-responsive" id="form_step3">
					<table class="table table-hover" id="tb_step_3">
						<thead>
							<tr>
								<!-- <th width='5%'></th> -->
								<th>
									<label>
										<input type="checkbox" id="toggle_all">
										Select Field
									</label>
								</th>
								<th width='20%'>Rename</th>
								<th width='25%'>Operator</th>
								<th width='20%'>Description</th>
								<th width='2%'></th>
							</tr>
						</thead>

						<tbody>
							<?php
							$operators = [
								[
									'label' => 'Equal (=)',
									'value' => 'equal',
								],
								[
									'label' => 'Not Equal (!=)',
									'value' => 'not_equal',
								],
								[
									'label' => 'Greather than (>)',
									'value' => 'greate_than',
								],
								[
									'label' => 'Less than (<)',
									'value' => 'less_than',
								],
								[
									'label' => 'Greather than or equal (>=)',
									'value' => 'greate_equal',
								],
								[
									'label' => 'Less than or equal (<=)',
									'value' => 'less_equal',
								],
								[
									'label' => 'Search for a pattern (LIKE)',
									'value' => 'like',
								],
								[
									'label' => 'Search for not pattern (NOT LIKE)',
									'value' => 'not_like',
								],
							];

							if( !$rest->schema_req_markup ){
								foreach( $columns as $cl ){
									echo "<tr>";
									echo 	"<td>";
									echo 		"<label>";
									echo 			"<input type='checkbox' name='active[".$cl."]'> ";
									echo 			$cl;
									echo 		"</label>";
									echo 	"</td>";
									echo 	"<td><input type='text' class='form-control input_rename alias_str' name='rename[".$cl."]'></td>";
									echo 	"<td>";
									echo 		"<select class='form-control' name='operator[".$cl."]'>";
		
									foreach( $operators as $op )
										echo "<option value='".$op['value']."'>".$op['label']."</option>";

									echo 		"</select>";
									echo 	"</td>";
									echo 	"<td><textarea class='form-control' rows='1' name='desc[".$cl."]'></textarea></td>";
									echo 	"<td>";
									echo 		"<i class='fa fa-arrows'></i>";
									echo 		"<input type='hidden' name='cl_name[]' class='cl_name' value='".$cl."'>";
									echo 	"</td>";
									echo "</tr>";
								}
							}else{

								foreach( $configs->configs as $cl ){

									$checked = '';
									if( $cl->active == 1 )
										$checked = 'checked';

									echo "<tr>";
									echo 	"<td>";
									echo 		"<label>";
									echo 			"<input type='checkbox' name='active[".$cl->name."]' ".$checked."> ";
									echo 			$cl->name;
									echo 		"</label>";
									echo 	"</td>";
									echo 	"<td><input type='text' class='form-control input_rename alias_str' name='rename[".$cl->name."]' value='".$cl->rename."'></td>";
									echo 	"<td>";
									echo 		"<select class='form-control' name='operator[".$cl->name."]'>";

									foreach( $operators as $op ){
										$selected = '';
										if( $op['value'] == $cl->operator )
											$selected = 'selected';

										echo "<option value='".$op['value']."' ".$selected.">".$op['label']."</option>";
									}

									echo 		"</select>";
									echo 	"</td>";
									echo 	"<td><textarea class='form-control' rows='1' name='desc[".$cl->name."]'>".$cl->desc."</textarea></td>";
									echo 	"<td>";
									echo 		"<i class='fa fa-arrows'></i>";
									echo 		"<input type='hidden' name='cl_name[]' class='cl_name' value='".$cl->name."'>";
									echo 	"</td>";
									echo "</tr>";
								}
							}
							?>
						</tbody>
					</table>
				</form>
			</div>
			<div class="col-md-4">
				<h6>ตัวอย่างการเรียกข้อมูล</h6>
				<div class="pretty_code">
					<p class="mb-0">
						curl -X POST \<br>
						&nbsp;'{{ asset('') }}api/{{ $rest->api_url_schema }}' \<br>
						&nbsp;-H 'accept: application/json' \<br>
						<?php
						if( $rest->api_type == 'private' )
							echo "&nbsp;-H 'authorization: Bearer YOUR_ACCESS_TOKEN' ";
						?>
					</p>
				</div>
				
				<h6>ตัวอย่าง JSON Request Data</h6>
				<!-- <textarea rows="5" class="form-control" id="pretty_json"></textarea> -->
				<pre id="pretty_json"></pre>

				<h6>ตัวอย่าง XML Response Data</h6>
				<pre id="pretty_xml">
					<?xml version="1.0" encoding="utf-8"?> <testysqltwo></testysqltwo>
				</pre>
			</div>
		</div>
	
	</div>

	<div class="card-footer">
		<a href="{{ route('backend.rest.step', [$rest->id, 2]) }}" class="btn btn-secondary btn_form">Cancel</a>
		<div class="pull-right">
			<button type="button" class="btn btn-secondary btn_form btn_save">Save</button>
			<button type="button" class="btn btn-secondary btn_form btn_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>

			<button type="button" class="btn btn_pink btn_form btn_next">Save & Next</button>
			<button type="button" class="btn btn_pink btn_form btn_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
		</div>
	</div>

</div>
@endsection

@push('add_js')
<script src="{{ asset('plugins/jQuery-sortable/jquery-sortable.js') }}"></script>

<script>

$('.input_rename').removeAttr('autocomplete');

set_example_json();
$("#tb_step_3 tbody tr td input[type=checkbox]").change(function(e){
	set_disable_input();
	set_example_json();
});
$("#tb_step_3 tbody tr td:nth-child(1n) input[type=text]").keyup(function(){
	set_example_json();
});

function set_example_json()
{
	var json_rest = {};
	var _tr = $("#tb_step_3 tbody tr");
	// console.log(_tr.length);
	for( let i=0; i<_tr.length; i++ ){
		// console.log(i);
		var tr_eq = $("#tb_step_3 tbody tr").eq(i);
		var chk = tr_eq.find('td input[type=checkbox]');
		if( chk.is(':checked') ){

			let key = tr_eq.find('td:eq(1) input[type=text]').val();
			if( key == '' )
				key = tr_eq.find('.cl_name').val();

			json_rest[key] = '...';
		}
	}

	$('#pretty_json').html( syntaxHighlight(json_rest) );

	let xml_rest = '&lt;?xml version="1.0" encoding="utf-8"?&gt;<br>';
	xml_rest += '&lt;{{ $rest->api_url_schema }}&gt;';
	// xml_rest +=		'<br>&nbsp;&nbsp;&lt;row_0&gt;';
	Object.keys(json_rest).forEach(key => {
	 	xml_rest += '<br>&nbsp;&nbsp;&nbsp;&nbsp;&lt;'+key+'&gt;...&lt;/'+key+'&gt;';
	});
	// xml_rest +=		'<br>&nbsp;&nbsp;&lt;/row_0&gt;';
	xml_rest += '<br>&lt;/{{ $rest->api_url_schema }}&gt;';

	$('#pretty_xml').html( xml_rest );
}


$("#toggle_all").change(function(){
	if( $(this).is(':checked') )
		toggle_all('checked');
	else
		toggle_all('uncheck');
});

function toggle_all(type)
{
	var json_rest = {};
	var _tr = $("#tb_step_3 tbody tr");
	// console.log(_tr.length);
	for( let i=0; i<_tr.length; i++ ){
		var tr_eq = $("#tb_step_3 tbody tr").eq(i);
		let as = tr_eq.find('td input[type=checkbox]');
		// console.log(as);

		if( type == 'checked' )
			as.prop( "checked", true ).change();
		else
			as.prop( "checked", false ).change();
	}
}


$("#tb_step_3").sortable({
	containerSelector: 'table',
  	itemPath: '> tbody',
  	itemSelector: 'tr',
  	onDrop: function(res){
  		set_example_json();
  	}
});


set_disable_input();
function set_disable_input()
{
	var _tr = $("#tb_step_3 tbody tr");
	// console.log(_tr.length);
	for( let i=0; i<_tr.length; i++ ){
		// console.log(i);
		var tr_eq = $("#tb_step_3 tbody tr").eq(i);
		if( !tr_eq.find('td input[type=checkbox]').is(':checked') ){
			tr_eq.find('td:eq(1) input[type=text], td:eq(3) textarea').prop('readonly', true);
			tr_eq.find('td:eq(2) select').prop('disabled', true);
		}else{
			tr_eq.find('td:eq(1) input[type=text], td:eq(3) textarea').prop('readonly', false);
			tr_eq.find('td:eq(2) select').prop('disabled', false);
		}
	}
}


$("#search_column").keyup(function(){
	let search_text = $("#search_column").val().toLowerCase();

	var _tr = $("#tb_step_3 tbody tr");
	// console.log(_tr.length);
	for( let i=0; i<_tr.length; i++ ){
		_tr.eq(i).css({display: 'none'});
		let cl_name = _tr.eq(i).find('.cl_name').val().toLowerCase();
		if( cl_name.includes(search_text) ){
			_tr.eq(i).css({display: 'table-row'})
		}
	}
});


// Submit form
$('.btn_save').click(function(){
	form_submit(0);
});

$('.btn_next').click(function(){
	form_submit(1);
});

function form_submit(let_next)
{
	var chk_duplicate = [];
	var _tr = $("#tb_step_3 tbody tr");
	for( let i=0; i<_tr.length; i++ ){
		var tr_eq = $("#tb_step_3 tbody tr").eq(i);

		let rename = tr_eq.find('td:eq(1) input[type=text]').val();
		if( rename != '' ){
			if( !chk_duplicate.includes(rename) ){
				chk_duplicate.push(rename);
			}else{
				swal({
					type: 'warning',
					title: 'Warning !',
					text: "Rename duplicate !",
				});
				return false;
			}
		}

		if( rename == 'page' ){
			swal({
				type: 'warning',
				title: 'Warning !',
				text: 'Do not use Rename "page"',
			});
			return false;
		}
	}
	

	$(".btn_save, .btn_next").hide();
	$(".btn_loading").show();

	var formData = new FormData($("#form_step3")[0]);
	formData.append('condition', $('#condition').val());

	$.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		type: 'post',
		url: "{{ route('backend.rest.step3_save', $rest->id) }}",
		data: formData,
		processData: false,
		contentType: false,
		success: function(res)
		{
			console.log(res);
			$(".btn_save, .btn_next").show();
			$(".btn_loading").hide();

			if( res.status == 'success' ){
				swal({
					type: "success",
					title: 'Updated',
				})
				.then(() => {
					if( let_next == 1 )
						window.location.href = "{{ route('backend.rest.step', [$rest->id, 4]) }}";
					else
						window.location.reload();
				});

			}else{
				var html = html_alert(res.data);
				// $('#ajax_validate').html(html);
			}
		},
		error: function (jqXHR, exception)
	    {
	    	$(".btn_save, .btn_next").show();
			$(".btn_loading").hide();
	        let msg = set_ajax_error(jqXHR, exception);
	        swal({
				type: 'error',
				title: 'Error',
				text: msg,
			});
	    },
	});
}




</script>
@endpush
