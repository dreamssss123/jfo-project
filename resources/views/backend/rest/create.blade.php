
@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Create Schema</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<form method="post" action="javascript:" id="form_db_connect">
			@csrf
			
			@include('backend.rest.form1')

			<!-- <div class="text-right">
				<a href="{{ route('backend.rest.index') }}" class="btn btn-secondary btn_form">Cancel</a>
				&nbsp;
				<button type="submit" class="btn btn_pink btn_form btn_dbconnect_default">Save</button>
				<button type="submit" class="btn btn_pink btn_form btn_dbconnect_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
			</div> -->
		</form>

	</div>
</div>
@endsection