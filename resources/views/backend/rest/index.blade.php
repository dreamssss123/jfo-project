
@extends("backend/layouts/backend")


@push('add_css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}">
<style>
.dataTables_filter{display: none;}
</style>
@endpush

@section('content')
<?php
$perms = \App\Perm::can();
?>

<div class="section-header">
	<h2>
		API
		<div class="pull-right">
			@if( $perms[5]->create )
			<a href="{{ route('backend.rest.create') }}" class="btn btn-sm btn_pink"><i class="fa fa-plus"></i>&nbsp; Create schema</a>
			@endif
		</div>
	</h2>
</div>

<div class="card">
	<div class="card-body">

		@include('layouts.errors')

		<div class="row">
			<div class="col-md-3">
				<input type="text" class="form-control" id="seach_name" placeholder="Search API name">
			</div>

			<div class="col-md-3">
				<select class="form-control" id="search_type">
		            <option value=>Database Connection</option>
		            <option value="mysql">MySQL</option>
		            <option value="sqlsrv">MSSQL</option>
		            <option value="oracle">Oracle</option>
		        </select>
			</div>

			<div class="col-md-3">
				<select class="form-control" id="search_status">
		            <option value=>Status</option>
		            <option value="1">Enable</option>
		            <option value="0">Disable</option>
		        </select>
			</div>
		</div>

		<div class="table-responsive">
			{!! $dataTable->table(['id'=> 'tb_main', 'class'=> 'table table-bordered table-hover wid100'], false) !!}
		</div>

	</div>
</div>

@endsection


@push('add_js')
<!-- <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script> -->
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<!-- <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script> -->
<!-- <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('../vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side.js') }}"></script> -->
{!! $dataTable->scripts() !!}
@endpush
