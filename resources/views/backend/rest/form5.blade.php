
@extends("backend/layouts/backend")


@push('add_css')
<style>

</style>
@endpush

@section('content')

<?php

if( $rest->schema_res_markup ){
	$configs = json_decode($rest->schema_res_markup);
	// echo $rest->schema_res_markup;
	// print_r($configs);
}

?>
<div class="section-header">
	<h2>Create Schema</h2>
</div>

<div class="card noedge">
	
	@include('backend.rest.nav_step', ['step' => $step])
	
	<div class="card-body pb-5">
		
		<h1 class="text-center mt-5 mb-5">Create API schema completed</h1>

		<p class="text-center">API : {{ asset('') }}api/{{ $rest->api_url_schema }}</p>

		<div class="text-center">
			<a href="{{ asset('docs/Api-Doc-'.$rest->id.'.docx') }}" class="btn btn-primary" download>Export .DOCX</a>
		</div>
	</div>

	<div class="card-footer">
		<div class="pull-right">
			<a href="{{ route('backend.index') }}" class="btn btn_pink">Go to Dashboard</a>
		</div>
	</div>

</div>
@endsection

