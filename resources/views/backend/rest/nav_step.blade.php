
<?php
$arrow_step = "<img src='".asset('images/arrow-step3.png')."' class='arrow_step'>";

// @$rest;
$has_url1 = 0;
if( @$rest ){
	$has_url1 = 1;
	$url1 = route('backend.rest.step', [$rest->id, 1]);
}

$has_url2 = 0;
if( $has_url1 == 1 && $rest->conn_table ){

	$has_url2 = 1;
	$url2 = route('backend.rest.step', [$rest->id, 2]);
}

$has_url3 = 0;
if( $has_url2 == 1 && $rest->api_url_schema ){

	$has_url3 = 1;
	$url3 = route('backend.rest.step', [$rest->id, 3]);
}

$has_url4 = 0;
if( $has_url3 == 1 && $rest->schema_req_markup ){

	$has_url4 = 1;
	$url4 = route('backend.rest.step', [$rest->id, 4]);
}

?>

<div class="row_step">
	<div class="sub_step step1 {{ $step>=1? 'step_active':'' }}">
		<a <?php echo $has_url1==1 ? "href='".$url1."'" : '';?>>Step 1 : Choose database connection</a>
		<?php echo $arrow_step;?>
	</div>
	<div class="sub_step step2 {{ $step>=2? 'step_active':'' }}">
		<a <?php echo $has_url2==1 ? "href='".$url2."'" : '';?>>Step 2 : Create API name</a>
		<?php echo $arrow_step;?>
	</div>
	<div class="sub_step step3 {{ $step>=3? 'step_active':'' }}">
		<a <?php echo $has_url3==1 ? "href='".$url3."'" : '';?>>Step 3 : Parameters</a>
		<?php echo $arrow_step;?>
	</div>
	<div class="sub_step step4 {{ $step>=4? 'step_active':'' }}">
		<a <?php echo $has_url4==1 ? "href='".$url4."'" : '';?>>Step 4 : Response format</a>
		<?php echo $arrow_step;?>
	</div>
	<div class="sub_step step5 {{ $step==5? 'step_active':'' }}">
		<a>Step 5 : Done & Export</a>
		<?php echo $arrow_step;?>
	</div>
</div>

