
@extends("backend/layouts/backend")


@push('add_css')
<style>
/*#step2_domain_span{display:inline-block; width:200px; vertical-align:middle;}
input[name=api_url_schema]{display:inline-block; width:calc(100% - 200px);}*/
#step2_domain_span{display:inline-block; width:auto; vertical-align:middle;}
input[name=api_url_schema]{display:inline-block; width:300px; vertical-align:middle;}
</style>
@endpush

@section('content')
<div class="section-header">
	<h2>Create Schema</h2>
</div>

<form method="post" action="javascript:" id="form_rest" class="card noedge">
	
	<?php
	$page_step = 1;
	if( @$step ) 
		$page_step = @$step;
	?>
	@include('backend.rest.nav_step', ['step' => $page_step])

	<div class="card-body" style="padding-bottom:0;">
		@include('layouts.errors')
		<div id="ajax_validate"></div>
	</div>

	<div class="card-body" id="form_step1" style="display:none;">

		<div>
			@csrf

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Database type :</label>
				<div class="col-sm-10">
			    	<select class="form-control" id="conn_type">
			    		<option>-- Database type --</option>
			    		<option value="mysql" {{ @$dbconn->conn_type=='mysql'? 'selected':'' }}>MySQL</option>
			    		<option value="sqlsrv" {{ @$dbconn->conn_type=='sqlsrv'? 'selected':'' }}>MSSQL</option>
			    		<option value="oracle" {{ @$dbconn->conn_type=='oracle'? 'selected':'' }}>Oracle</option>
			    	</select>
			    </div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Choose database connection :</label>
				<div class="col-sm-10">
			    	<select class="form-control" name="conn_alias">
			    		<option value>-- Choose Connection --</option>
			    	</select>
			    </div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label"></label>
				<div class="col-sm-10">
			    	<button type="button" class="btn btn-primary btn_test_connect">Connected</button>
			    	<button type="button" class="btn btn-primary btn_connect_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
			    	&nbsp;
			    	<span class="conenct_status"></span>
			    </div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Choose Table :</label>
				<div class="col-sm-10">
			    	<select class="form-control" name="conn_table">
			    		<option value>-- Choose Table --</option>
			    	</select>
			    </div>
			</div>


			<!-- <div class="text-right">
				<a href="{{ route('backend.rest.index') }}" class="btn btn-secondary btn_form">Cancel</a>
				&nbsp;
				<button type="submit" class="btn btn_pink btn_form btn_dbconnect_default">Save</button>
				<button type="submit" class="btn btn_pink btn_form btn_dbconnect_loading" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
			</div> -->
		</div>

	</div>


	
	<div class="card-body" id="form_step2" style="display:none;">
		
		<div class="form-group row label_status">
			<label class="col-sm-2 col-form-label">Authentication : </label>
			<div class="col-sm-10">
				<?php
				$checked = 'public';
				if( @$rest->api_type == 'private' )
					$checked = 'private';
				?>
		    	<label>
		    		<input type="radio" name="api_type" value="public" {{ $checked=='public'? 'checked':'' }}>
		    		Public API
		    	</label>
		    	&nbsp;
		    	<label>
		    		<input type="radio" name="api_type" value="private" {{ $checked=='private'? 'checked':'' }}>
		    		Private API
		    	</label>
		    </div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label">API name :</label>
			<div class="col-sm-10">
		    	<input type="text" class="form-control" name="api_name" value="{{ @$rest->api_name }}">
		    </div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label">API URL :</label>
			<div class="col-sm-10">
		    	{{-- <span id="step2_domain_span">https://apijfo.moj.go.th/api/</span><input type="text" class="form-control api_str" name="api_url_schema" value="{{ @$rest->api_url_schema }}" {{ @$rest->id? 'disabled':'' }}> --}}

		    	<span>{{ asset('') }}api/</span>
		    	<input type="text" class="form-control api_str" name="api_url_schema" value="{{ @$rest->api_url_schema }}" {{ @$rest->id? 'disabled':'' }}>
		    </div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Description :</label>
			<div class="col-sm-10">
		    	<textarea class="form-control" name="api_description" rows="4">{{ @$rest->api_description }}</textarea>
		    </div>
		</div>

		<?php
		$checked = 1;
		if( isset($rest->status) && $rest->status == 0 )
			$checked = 0;
		?>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Status :</label>
			<div class="col-sm-10">
		    	<label>
		    		<input type="radio" name="status" value="1" {{ $checked==1? 'checked':'' }}>
		    		Enable
		    	</label>
				&nbsp; &nbsp; &nbsp;
		    	<label>
		    		<input type="radio" name="status" value="0" {{ $checked==0? 'checked':'' }}>
		    		disable
		    	</label>
		    </div>
		</div>

	</div>

	<div class="card-footer btn_step1" style="display:none;">
		<a href="{{ route('backend.rest.index') }}" class="btn btn-secondary btn_form">Cancel</a>
		<div class="pull-right">
			<button type="button" class="btn btn_pink btn_form btn_submit1">Next</button>
		</div>
	</div>

	<div class="card-footer btn_step2" style="display:none;">
		<button type="button" class="btn btn-secondary btn_form backto_step1">Cancel</button>
		<div class="pull-right">
			<button type="button" class="btn btn_pink btn_form btn_submit2">Next</button>
			<button type="button" class="btn btn_pink btn_form btn_loading2" style="display:none;"><i class="fa fa-refresh fa-spin fa-fw"></i></button>
		</div>
	</div>

</form>
@endsection

@push('add_js')
<script>

$(document).ready(function(){


if( '{{ @$step }}' == '2' ){
	$('#form_step1, .btn_step1').hide();
	$('#form_step2, .btn_step2').show();
}else{
	$('#form_step1, .btn_step1').show();
	$('#form_step2, .btn_step2').hide();
}


// Edit
@if( @$dbconn->id )

set_connect();
async function set_connect()
{
	await list_connection();
	await $('select[name=conn_alias]').val('{{ @$rest->conn_alias }}');

	$('.btn_test_connect').hide();
	$('.btn_connect_loading').show();

	await list_tables();
	$('.btn_test_connect').show();
	$('.btn_connect_loading').hide();

	$('select[name=conn_table]').val('{{ @$rest->conn_table }}');
}
@endif
// End Edit



$('#conn_type').change(function(){
	list_connection();

	let conn = "<option value>Choose Connection</option>";
	$('select[name=conn_alias]').html(conn);

	let tb = "<option value>Choose Table</option>";
	$('select[name=conn_table]').html(tb);
	$('.conenct_status').html('');
});

async function list_connection()
{
	await $.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		type: 'post',
		url: "{{ route('backend.dbconn.list_connection') }}",
		data: {user_id: "{{ Auth::user()->id }}", conn_type: $('#conn_type').val()},
		success: function(res)
		{
			console.log(res);
			let html = '';
			if( res.length > 0 ){
				$.each(res, function(index, val){
					html += "<option value='"+val.conn_alias+"'>"+val.conn_name+"</option>";
				});
			}else{
				html = "<option value>Choose Connection</option>";
			}

			$("select[name=conn_alias]").html(html);
		}
	});
}


$("select[name=conn_alias]").change(function(){
	let html = "<option value>Choose Table</option>";
	$('select[name=conn_table]').html(html);
	$('.conenct_status').html('');
});

$('.btn_test_connect').click(async function(){
	
	if( $("select[name=conn_alias]").val() == '' ){
		swal({
			type: 'warning',
			title: 'Please select Connection.',
		});
		return false;
	}

	$('.btn_test_connect').hide();
	$('.btn_connect_loading').show();

	await list_tables();
	$('.btn_test_connect').show();
	$('.btn_connect_loading').hide();
});

async function list_tables()
{
	$('.btn_submit2').hide();
	$('.btn_loading2').show();

	await $.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		type: 'post',
		url: "{{ route('backend.dbconn.connect_by_alias') }}",
		data: {conn_alias: $("select[name=conn_alias]").val()},
		success: function(res)
		{
			console.log(res);
			$('.btn_submit2').show();
			$('.btn_loading2').hide();
			
			if( res.status == 'success' ){

				$('.conenct_status').html("<span class='text-success'>Connect success !</span>");
				let html = '';
				if( res.tables.length == 0 ){
					html += "<option value>-- Choose Table --</option>";
				}else{
					$.each(res.tables, function(index, val){
						html += "<option value='"+val+"'>"+val+"</option>";
					});
				}

				$("select[name=conn_table]").html(html);

			}else{
				swal({
					type: 'error',
					title: 'Error',
					text: res.msg,
				});
				$('.conenct_status').html("<span class='text-danger'>Connection fail !</span>");
			}

		},
		error: function (jqXHR, exception)
	    {
	    	$('.btn_submit2').show();
			$('.btn_loading2').hide();

	        let msg = set_ajax_error(jqXHR, exception);
	        swal({
				type: 'error',
				title: 'Error',
				text: msg,
			});
	    },
	});
}



$('.btn_submit1').click(function(){

	var conn_table = $("select[name=conn_table]");
	if( conn_table.val().trim() == '' ){
		swal({
			type: 'warning',
			title: 'Warning',
			text: 'Please Choose Table',
		});
	}else{

		$("#form_step1, .btn_step1").hide();

		$('.step2').addClass('step_active');
		$("#form_step2, .btn_step2").show();
	}
});

$('.backto_step1').click(function(){
	$("#form_step1, .btn_step1").show();
	$('.step2').removeClass('step_active');
	$("#form_step2, .btn_step2").hide();
});



$('.btn_submit2').click(async function(){

	@if( @$rest->schema_req_markup )
		let status = await Swal.fire({
			type: "warning",
			title: 'Wraning',
			text: 'ข้อมูล step 3 และ 4 จะหายไป ยืนยันหรือไม่',
			showCancelButton: true,
			confirmButtonText: 'Confirm'
		})
		.then((res)=>{
			if ( res.value ){
				return true;
			}else{
				return false;
			}
		});

		if( status == false ){
			return false;
		}
	@endif

	$('.btn_submit2').hide();
	$('.btn_loading2').show();

	var formData = new FormData($("#form_rest")[0]);
	@if( @$rest->id )
	let url = "{{ route('backend.rest.update_firststep', $rest->id) }}";
	@else
	let url = "{{ route('backend.rest.store') }}";
	@endif


	$.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		type: 'post',
		url: url,
		data: formData,
		processData: false,
		contentType: false,
		success: function(res)
		{
			// console.log(res);
			$('.btn_submit2').show();
			$('.btn_loading2').hide();

			if( res.status == 'success' ){
				swal({
					type: "success",
					title: 'Success',
					text: 'API Schema Created.',
				})
				.then(function(){
					window.location.href = res.url;
				});

			}else{
				var html = html_alert(res.data);
				$('#ajax_validate').html(html);
			}
		},
		error: function (jqXHR, exception)
	    {
	    	$('.btn_submit2').show();
			$('.btn_loading2').hide();
	        let msg = set_ajax_error(jqXHR, exception);
	        swal({
				type: 'error',
				title: 'Error',
				text: msg,
			});
	    },
	});
});

});

</script>
@endpush
