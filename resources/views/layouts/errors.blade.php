
@if(!$errors->has('login'))
  @if($errors->all())
      <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <ul>
              @foreach ($errors->all() as $message)
                  <li><?php echo $message;?></li>
              @endforeach
          </ul>
      </div>
  @endif
@endif
@if(Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p><?php echo Session::get('message');?></p>
    </div>
@endif
@if(Session::has('warning'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
            <li><?php echo Session::get('warning');?></li>
        </ul>
    </div>
@endif

@if(Session::has('danger'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
            <li><?php echo Session::get('danger');?></li>
        </ul>
    </div>
@endif

@if(Session::has('flash_warning'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p><?php echo Session::get('flash_warning');?></p>
    </div>
@endif


@section('add_error')
<script type="text/javascript">
$(document).ready(function () {
  //swal("Fail to Login!", "Please try again.", "error");
});

function getErrors(title,message) {
  Swal({
    title: title,
    text: message,
    type: "error",
    confirmButtonText: "Close",
  });
}

function getWarning(title,message) {
  Swal({
    title: title,
    text: message,
    type: "warning",
    confirmButtonText: "Close",
  });
}

function getSucess(title,message) {
  Swal({
    title: title,
    text: message,
    type: "success",
    confirmButtonText: "Close",
  });
}

function getInfo(title,message) {
  Swal({
    title: title,
    text: message,
    type: "info",
    confirmButtonText: "Close",
  });
}

function delConfirm() {
  Swal({
		text: "Are you sure you want to delete this?",
		type: "warning",
		confirmButtonText: 'Yes, delete it!',
		confirmButtonColor: '#d33',
		showCancelButton: true,
	})
	.then((result) => {
		return result.value;
	});
}

</script>
@endsection
