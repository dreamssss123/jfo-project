<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="{{ asset('images/icon/favicon-32x32.png') }}"/>
<meta name="Author" content="Napat Pongsawat">
<meta name="description" content="ระบบเชื่อมโยงข้อมูล JFO API gateway">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }}</title>

<!-- v4.0.0-alpha.6 -->
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/et-line-font/et-line-font.css') }}">
<link rel="stylesheet" href="{{ asset('css/themify-icons/themify-icons.css') }}">

<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/temp.css') }}">


@stack('add_css')

<script>
var home_url = "{{ url('') }}";
</script>

</head>

<body>


@yield('frame')


<!-- jQuery 3 -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Tether -->
<script src="{{ asset('plugins/tether-1.3.3/dist/js/tether.min.js') }}"></script>
<!-- v4.0.0-alpha.6 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/sweetalert2.js') }}"></script>

<!-- template --> 
<script src="{{ asset('js/niche.js') }}"></script> 

<script src="{{ asset('js/main.js') }}"></script> 


@stack('add_js')
   
</body>
</html>
