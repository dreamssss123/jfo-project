
<?php
$c = new Carbon\Carbon;
$setting = \App\Configuration::where('name', 'api')->first();
?>

<p>{{ $c->now()->format('d/m/Y H:i:s') }}</p>
<p>Request API "api/{{ $api }}" reject over {{ $setting->global_quantity }} time.</p>