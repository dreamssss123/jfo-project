-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 03:27 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `parent`, `name`, `sort`) VALUES
(1, 0, 'Dashboard', 1),
(2, 0, 'User', 2),
(3, 0, 'Partner', 3),
(4, 0, 'Database Connection', 5),
(5, 0, 'API', 6),
(6, 0, 'Report', 7),
(7, 0, 'Log', 8),
(8, 0, 'Setting', 9),
(9, 0, 'Organize Partner', 4);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_09_11_152936_create_permission_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `can_view` int(1) NOT NULL,
  `can_create` int(1) NOT NULL,
  `can_edit` int(1) NOT NULL,
  `can_delete` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `role_id`, `menu_id`, `can_view`, `can_create`, `can_edit`, `can_delete`, `created_at`, `updated_at`) VALUES
(65, 12, 1, 1, 1, 1, 1, '2019-09-20 08:13:55', '2019-09-20 10:36:35'),
(66, 12, 2, 1, 1, 1, 1, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(67, 12, 3, 1, 1, 1, 1, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(68, 12, 9, 1, 1, 1, 1, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(69, 12, 4, 0, 0, 0, 0, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(70, 12, 5, 0, 0, 0, 0, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(71, 12, 6, 0, 1, 1, 0, '2019-09-20 08:13:55', '2019-09-20 10:23:20'),
(72, 12, 7, 0, 0, 0, 0, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(73, 12, 8, 0, 0, 0, 0, '2019-09-20 08:13:55', '2019-09-20 08:13:55'),
(74, 13, 1, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(75, 13, 2, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(76, 13, 3, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(77, 13, 9, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(78, 13, 4, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(79, 13, 5, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(80, 13, 6, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(81, 13, 7, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34'),
(82, 13, 8, 1, 1, 1, 1, '2019-09-20 08:14:34', '2019-09-20 08:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(12, 'Admin', 1, '2019-09-20 08:13:55', '2019-09-20 08:43:35'),
(13, 'Super Admin', 1, '2019-09-20 08:14:34', '2019-09-20 08:47:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role_id`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'OFza123', 'ofaion123@gmail.com', 12, NULL, '$2y$10$FsC7SBXtuOXbhIcwWKvq7uh2qA.5R5V3c/uomz6YU6O3UuBF1wr8C', NULL, '2019-09-16 08:39:16', '2019-09-16 08:39:16'),
(7, 'asdasdsa', 'test1@gmail.com', 0, NULL, '$2y$10$fJxDxBMoAoHh0.PUlLrj0OepG/127qIx3wL5w1bablbkz9UixPjFi', NULL, '2019-09-17 09:54:54', '2019-09-17 09:54:54'),
(8, 'asdasdsa', 'test2@gmail.com', 0, NULL, '$2y$10$KTjEQEWs9PTWJzpXT9U42OWmt235uqF4pIS000p.5He95O8v929Kq', NULL, '2019-09-17 09:55:05', '2019-09-17 09:55:05'),
(9, 'asdasdsa', 'test3@gmail.com', 0, NULL, '$2y$10$EupUeI/TPLx8fgD7Da3EEewAr6Gh6Tq54yyzf7GQAW3SxssOr/ao.', NULL, '2019-09-17 09:55:09', '2019-09-17 09:55:09'),
(10, 'asdasdsa', 'test4@gmail.com', 0, NULL, '$2y$10$eIgwRPdme3ElLeaWANZpv.7OYoZyS9KjCaZCZlh/h5tpRoEHUntdu', NULL, '2019-09-17 09:55:11', '2019-09-17 09:55:11'),
(11, 'asdasdsa', 'test5@gmail.com', 0, NULL, '$2y$10$4nX7DZGge4/7S1ub37PzjuJDEW0E3etIqO3i1yxgYhNEE7WfLQMdm', NULL, '2019-09-17 09:55:13', '2019-09-17 09:55:13'),
(12, 'asdasdsa', 'test6@gmail.com', 0, NULL, '$2y$10$0bA85gaR4ROMT.db7McNxeMCm6gYHj28TZ6d6XzFKk1C.1MSdmyN6', NULL, '2019-09-17 09:55:16', '2019-09-17 09:55:16'),
(13, 'asdasdsa', 'test7@gmail.com', 0, NULL, '$2y$10$zKXRxUd1JcCWD945iqKiheTU6MqP.oN/15FR18lF/F.cQgrPi/lhm', NULL, '2019-09-17 09:55:19', '2019-09-17 09:55:19'),
(14, 'asdasdsa', 'test8@gmail.com', 0, NULL, '$2y$10$jSzGDGAtONcGAPlw62q7deKt6JNB4cI0zh8Iq8ih60Q80wROQpDPS', NULL, '2019-09-17 09:55:47', '2019-09-17 09:55:47'),
(15, 'asdasdsa', 'test9@gmail.com', 0, NULL, '$2y$10$9mL1.HfBpabrapRONBt6KO4YdFO40dA3XFj6XC8jHMXXEi8acFRaK', NULL, '2019-09-17 09:55:49', '2019-09-17 09:55:49'),
(16, 'asdasdsa', 'test10@gmail.com', 0, NULL, '$2y$10$c8sNzpq3KEzh65jrbSVSjeugwh35u99fRZin.9EVKVUUpWnNS4jcu', NULL, '2019-09-17 09:55:51', '2019-09-17 09:55:51'),
(17, 'asdasdsa', 'test11@gmail.com', 0, NULL, '$2y$10$yrv9EkeBw23vJ2l4n32uzub.aHt140ngN30dfqmHb/Mg97pviS5N2', NULL, '2019-09-17 09:55:54', '2019-09-17 09:55:54'),
(18, 'asdasdsa', 'test12@gmail.com', 12, NULL, '$2y$10$d7HVRujGOaw786XLcUxOaeoXrUF7QKnf.PSKkMCA.OzePlaUrhyLu', NULL, '2019-09-17 09:55:56', '2019-09-17 09:55:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
