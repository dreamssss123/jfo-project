
$('.num_only').keypress(function (event) {
    // console.log(event.which);
    if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && event.which != 46) {
        event.preventDefault();
    }
});

$(".alias_str").keypress(function(event){
    var ew = event.which;
    // if(ew == 32) // space
    //     return true;
    if(48 <= ew && ew <= 57)
        return true;
    if(65 <= ew && ew <= 90)
        return true;
    if(97 <= ew && ew <= 122)
        return true;
    return false;
});

$(".api_str").keypress(function(event){
    var ew = event.which;
    // if(ew == 32) // space
    //     return true;
    // if(48 <= ew && ew <= 57)
    //     return true;
    if(65 <= ew && ew <= 90)
        return true;
    if(97 <= ew && ew <= 122)
        return true;
    if(ew == 95) // Underscore
    	return true;

    return false;
});




function addCommas(intNum) {
  return (intNum + '').replace(/(\d)(?=(\d{3})+$)/g, '$1,');
}



function set_ajax_error(jqXHR, exception){
	var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
    }

    console.log(msg);
    return msg;
}


function html_alert(data){
	var html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
	html += 		"<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
	html += 			"<span aria-hidden='true'>&times;</span>";
	html += 		"</button>";
	html += 		"<ul>";

	$.each(data, function(index, val){
		$.each(val, function(index2, val2){
			html += "<li>"+val2+"</li>";
		});
	});

	html += 		"</ul>";
	html += 	"</div>";

	return html;
}


/*********** Delete File Attach **************/
function del_file_custom(url, column, folder, refresh)
{
	swal({
	  	title: 'ยืนยันที่จะลบ ?',
	  	// text: "You won't be able to revert this!",
	  	type: 'warning',
	  	showCancelButton: true,
	  	confirmButtonColor: '#d33',
	  	cancelButtonColor: '#868e96',
	  	confirmButtonText: 'ยืนยัน',
	  	cancelButtonText: 'ยกเลิก',
	}).then(function(result){
	  	if (result.value) {
	  		$.ajax({
	  			url: url,
	  			type: "post",
	  			data: {column: column, folder: folder},
	  			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	  			success: function(res)
	  			{
	  				console.log(res);
	  				if( res.status == 'success' ){

	  					$("#box_btn_"+column).remove();
	  					$('input[name='+column+']').data('dropify').resetPreview();
						$('input[name='+column+']').data('dropify').clearElement();

						if( refresh != '' ){
							location.href = refresh;
						}

	  				}else{
	  					swal('error', 'Cannot delete file.', 'error');
	  				}
	  			}
	  		});
	  	}
	});
}



function del_attach(name_input, folder)
{
	swal({
	  	title: 'ยืนยันที่จะลบ ?',
	  	// text: "You won't be able to revert this!",
	  	type: 'warning',
	  	showCancelButton: true,
	  	confirmButtonColor: '#d33',
	  	cancelButtonColor: '#868e96',
	  	confirmButtonText: 'ยืนยัน',
	  	cancelButtonText: 'ยกเลิก',
	}).then(function(result){
	  	if (result.value) {
	  		$.ajax({
	  			url: home_url+"/member/del_attach",
	  			type: "post",
	  			data: {name_input: name_input, folder: folder},
	  			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
	  			success: function(res)
	  			{
	  				console.log(res);
	  				if( res.status == 'success' ){

	  					$("#box_btn_"+name_input).remove();
	  					$('input[name='+name_input+']').data('dropify').resetPreview();
						$('input[name='+name_input+']').data('dropify').clearElement();

	  				}else{
	  					swal('error', 'Cannot delete file.', 'error');
	  				}
	  			}
	  		});
	  	}
	});
}


$("body").on('click', '.btn_confirm_del_table', function(){
	var _this = $(this);
	swal({
	  	title: 'ยืนยันที่จะลบ ?',
	  	// text: "You won't be able to revert this!",
	  	type: 'warning',
	  	showCancelButton: true,
	  	confirmButtonColor: '#d33',
	  	cancelButtonColor: '#868e96',
	  	confirmButtonText: 'ยืนยัน',
	  	cancelButtonText: 'ยกเลิก',
	}).then(function(result){
	  	if (result.value) {
	  		_this.parent('form').submit();
	  	}
	});
});

$("body").on('click', '.btn_confirm_warning', function(){
	var _this = $(this);
	var title = _this.attr('data-title');
	if( typeof title === typeof undefined || title === false ){
		title = "ยืนยัน";
	}

	var text = _this.attr('data-text');
	if( typeof text === typeof undefined || text === false ){
		text = "";
	}

	swal({
	  	title: title,
	  	text: text,
	  	type: 'warning',
	  	showCancelButton: true,
	  	confirmButtonColor: '#d33',
	  	cancelButtonColor: '#868e96',
	  	confirmButtonText: 'ยืนยัน',
	  	cancelButtonText: 'ยกเลิก',
	}).then(function(result){
	  	if (result.value) {
	  		_this.parent('form').submit();
	  	}
	});
});

$("body").on('click', '.btn_confirm_submit', function(){
	var _this = $(this);
	var title = _this.attr('data-title');
	if( typeof title === typeof undefined || title === false ){
		title = "ยืนยัน";
	}
	
	swal({
	  	title: title,
	  	type: 'warning',
	  	showCancelButton: true,
	  	confirmButtonColor: '#09418a',
	  	cancelButtonColor: '#868e96',
	  	confirmButtonText: 'ยืนยัน',
	  	cancelButtonText: 'ยกเลิก',
	}).then(function(result){
	  	if (result.value) {
	  		_this.parent('form').submit();
	  	}
	});
})


function printDiv(divName){
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}


// Check ID Card
function checkID(id)
{
	if( id.length != 13 )
		return false;

	for( i=0, sum=0; i < 12; i++ )
		sum += parseFloat(id.charAt(i))*(13-i);

	if( (11-sum%11)%10 != parseFloat(id.charAt(12)) )
		return false;

	return true;
}



function tc_check(selector)
{
	selector.css('display', 'none');
	var html_check_box = "<div class='tc_check'><i class='tchk_i' style='display:none;'>&#10004;</i></div>";
	$(html_check_box).insertAfter(selector);

	if( selector.is(':checked') ){
		selector.next(".tc_check").find('i').show();
	}
}


// Payment File
function toggle_btn_load(type)
{
	if( type == 1 ){
		$(".payment_btn_submit").hide();
		$(".payments_btn_load").show();
	}else{
		$(".payment_btn_submit").show();
		$(".payments_btn_load").hide();
	}
}



// Display Pretty Json in <pre>
function syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}
