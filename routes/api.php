<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::group([
//     'middleware' => 'api',
//     'prefix' => 'auth'

// ], function ($router) {

//     Route::post('login', 'AuthController@login');
//     Route::post('logout', 'AuthController@logout');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::post('me', 'AuthController@me');
// });



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('api_xml', 'Api\TestXmlController@index');
Route::post('xml_test123', 'Api\TestXmlController@xml_test123');
Route::get('res_xml/{qry}', 'Api\TestXmlController@res_xml');
Route::get('res_json/{qry}', 'Api\TestXmlController@res_json');



Route::group([
    'prefix' => 'auth'

], function ($router) {
	Route::post('login', 'Api\ApiController@login');
    // Route::post('logout', 'Api\ApiController@logout');
    // Route::post('refresh', 'Api\ApiController@refresh');
    Route::post('me', 'Api\ApiController@me');
});

Route::middleware(['api_auth'])->group(function(){
	Route::post('/{schema}/{is_xml?}', 'Api\ApiController@main');
});



