<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('test_xml', 'TestXmlController@index');
Route::get('test_export_word', 'TestXmlController@test_export_word');
Route::get('test_send_mail', 'TestXmlController@test_send_mail');




//********* Backend *********//
Route::prefix('backend')->group(function () {
	Route::name('backend.')->group(function () {

		Route::get('auth/login', 'Backend\AuthController@login')->name('auth.login');
		Route::post('auth/login', 'Backend\AuthController@login_submit')->name('auth.login_submit');
		Route::get('auth/logout', 'Backend\AuthController@logout')->name('auth.logout');

		Route::middleware(['CheckAdminLogin'])->group(function () {
		    Route::get('/', 'Backend\DashboardController@index')->name('index');
		    
		    Route::resource('user', 'Backend\UserController');
		    Route::resource('partner_org', 'Backend\PartnerOrganizeController');
		    Route::resource('partner', 'Backend\PartnerController');
		    Route::get('partner/list_api/{id}', 'Backend\PartnerController@list_api')->name('partner.list_api');
		    Route::resource('dbconn', 'Backend\DatabaseConnectionController');
		    Route::post('dbconn/test_connect', 'Backend\DatabaseConnectionController@test_connect')->name('dbconn.test_connect');
		    Route::post('dbconn/list_connection', 'Backend\DatabaseConnectionController@list_connection')->name('dbconn.list_connection');
		    Route::post('dbconn/connect_by_alias', 'Backend\DatabaseConnectionController@connect_by_alias')->name('dbconn.connect_by_alias');
		    Route::resource('rest', 'Backend\RestController');
		    Route::post('rest/update_firststep/{id}', 'Backend\RestController@update_firststep')->name('rest.update_firststep');
		    Route::get('rest/{id}/step/{step}', 'Backend\RestController@step')->name('rest.step');
		    Route::post('rest/step3_save/{id}', 'Backend\RestController@step3_save')->name('rest.step3_save');
		    Route::post('rest/step4_save/{id}', 'Backend\RestController@step4_save')->name('rest.step4_save');


		    Route::resource('apilog', 'Backend\ApiLogController');
		    Route::post('apilog/export_data/{type}', 'Backend\ApiLogController@export_data')->name('apilog.export_data');
		    Route::resource('systemlog', 'Backend\SystemLogController');
		    Route::post('systemlog/export_data/{type}', 'Backend\SystemLogController@export_data')->name('systemlog.export_data');
		    Route::resource('report', 'Backend\ReportController');
		    Route::post('report/export_data/{type}', 'Backend\ReportController@export_data')->name('report.export_data');
		    Route::resource('setting', 'Backend\SettingController');


		    /**** Setting Role, Permission ****/
		    Route::resource('role', 'Backend\RoleController');
		});
	});
});
