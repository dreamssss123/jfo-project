<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
// use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
// use Yajra\DataTables\Html\Editor\Fields;
// use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\ApiGateways;

class RestPartnerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($row){

                $perms = \App\Perm::can();

                $html = '';

                // if( $perms[5]->delete ){
                //     $html .= "<form method='post' action='".route('backend.rest.destroy', $row->id)."' style='display:inline-block'>";
                //     $html .= csrf_field();
                //     $html .= "<input type='hidden' name='_method' value='DELETE'>";
                //     $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                //     $html .= "</form>";
                // }

                // $html .= " <a href='javascript:' class='btn btn-info btn-sm' download><i class='fa fa-download'></i></a>";

                return $html;
            })
            ->editColumn('api_url_schema', function($row){
                $html = 'api/ <span style="color:#000;">'.$row->api_url_schema.'</span>';

                return $html;
            })
            ->addIndexColumn()
            ->rawColumns(['api_url_schema', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\RestDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ApiGateways $model)
    {
        // print_r(request()->all());exit;
        $model = $model->newQuery()->select('id', 'api_name', 'api_url_schema')
                                    ->orderBy('update_at', 'desc');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    // ->setTableId('restdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    // ->dom('Bfrtip')
                    ->orderBy(1, 'asc')
                    ->parameters([
                        "pageLength" => 5,
                        'dom'          => 'Bfrtip',
                        'initComplete' => "function () {

                            this.api().columns(1).every(function () {
                                var column = this;
                                $('#search_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(2).every(function () {
                                var column = this;
                                $('#search_url').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                    ]);

                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'api_name', 'name'=> 'api_gateway.api_name', 'title' => 'API name'],
            ['data'=> 'api_url_schema', 'name'=> 'api_gateway.api_url_schema', 'title' => 'API URL'],
            // ['data'=> 'status', 'name'=> 'api_gateway.status', 'title' => 'Status'],
            // ['data'=> 'update_at', 'name'=> 'api_gateway.update_at', 'title' => 'Modified date'],
            // ['data'=> 'conn_type', 'name'=> 'api_connection.conn_type', 'title' => 'conn_type', 'visible' => false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Rest_' . date('YmdHis');
    }

}
