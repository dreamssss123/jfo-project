<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
// use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
// use Yajra\DataTables\Html\Editor\Fields;
// use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\ApiCategory;

class PartnerOrganizeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($row){

                $perms = \App\Perm::can();

                $html = '';

                if( $perms[9]->edit )
                    $html .= "<a href='".route('backend.partner_org.edit', $row->id)."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                if( $perms[9]->delete ){
                    $html .= "<form method='post' action='".route('backend.partner_org.destroy', $row->id)."' style='display:inline-block'>";
                    $html .= csrf_field();
                    $html .= "<input type='hidden' name='_method' value='DELETE'>";
                    $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                    $html .= "</form>";
                }

                return $html;
            })
            ->editColumn('status', function($row){
                if( $row->status  == 1 )
                    $html = "<span class='stat_success'>Enable</span>";
                else
                    $html = "<span class='stat_danger'>Disable</span>";

                return $html;
            })
            ->editColumn('update_at', function($row){
                $c = new Carbon;
                $parse = $c->parse($row->update_at);

                return $parse->format('d/m/Y H:i');
            })
            ->addIndexColumn()
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PartnerOrganizeDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ApiCategory $model)
    {
        return $model->newQuery()->select('id', 'cat_name', 'status')
                                ->orderBy('update_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    // ->setTableId('partnerorganizedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'initComplete' => "function () {

                            this.api().columns(1).every(function () {
                                var column = this;
                                $('#seach_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(2).every(function () {
                                var column = this;
                                $('#search_status').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                    ]);
                    
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'cat_name', 'name'=> 'cat_name', 'title' => 'Partner Organize name', 'orderable'=>false],
            ['data'=> 'status', 'name'=> 'status', 'title' => 'Status', 'orderable'=>false],
            ['data'=> 'update_at', 'name'=> 'update_at', 'title' => 'Modified date', 'orderable'=>false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions', 'orderable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PartnerOrganize_' . date('YmdHis');
    }
}
