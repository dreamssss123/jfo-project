<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
// use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
// use Yajra\DataTables\Html\Editor\Fields;
// use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\ApiGateways;

class RestDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($row){

                $user = \Auth::user();
                $perms = \App\Perm::can();

                $html = '';

                if( $user->role_id == config('app.sa_id') ){

                    $html .= "<a href='".route('backend.rest.step', [$row->id, 3])."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                    $html .= "<form method='post' action='".route('backend.rest.destroy', $row->id)."' style='display:inline-block'>";
                    $html .= csrf_field();
                    $html .= "<input type='hidden' name='_method' value='DELETE'>";
                    $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                    $html .= "</form>";

                }else{

                    if( $row->user_id == $user->id ){

                        if( $perms[5]->edit )
                            $html .= "<a href='".route('backend.rest.step', [$row->id, 3])."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                        if( $perms[5]->delete ){
                            $html .= "<form method='post' action='".route('backend.rest.destroy', $row->id)."' style='display:inline-block'>";
                            $html .= csrf_field();
                            $html .= "<input type='hidden' name='_method' value='DELETE'>";
                            $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                            $html .= "</form>";
                        }
                    }
                }

                return $html;
            })
            ->editColumn('api_url_schema', function($row){
                $html = 'api/ <span style="color:#000;">'.$row->api_url_schema.'</span>';

                return $html;
            })
            ->editColumn('status', function($row){
                if( $row->status  == 1 )
                    $html = "<span class='stat_success'>Enable</span>";
                else
                    $html = "<span class='stat_danger'>Disable</span>";

                return $html;
            })
            ->editColumn('update_at', function($row){
                $c = new Carbon;
                $parse = $c->parse($row->update_at);

                return $parse->format('d/m/Y H:i');
            })
            ->removeColumn('search')
            ->addIndexColumn()
            ->rawColumns(['api_url_schema', 'status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\RestDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ApiGateways $model)
    {
        // print_r(request()->all());exit;
        $model = $model->newQuery()->select('api_gateway.id', 'api_gateway.api_name', 'api_gateway.api_url_schema', 'api_gateway.status', 'api_gateway.update_at', 'api_connection.conn_type', 'api_connection.user_id')
                                    ->leftJoin('api_connection', 'api_connection.conn_alias', 'api_gateway.conn_alias')
                                    ->orderBy('api_gateway.update_at', 'desc');

        // if(  isset( request()['order'][0]['column'] ) && request()['order'][0]['column'] == 0 )
            // $model = $model->orderBy('update_at', 'desc');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    // ->setTableId('restdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    // ->dom('Bfrtip')
                    ->orderBy(1, 'asc')
                    ->parameters([
                        'dom'          => 'Bfrtip',
                        // 'bFilter' => false,
                        // 'columnDefs' => [
                        //     [
                        //         "targets" => 5,
                        //         "visible" => false,
                        //         // "searchable": false
                        //     ],
                        // ],
                        'initComplete' => "function () {
                            this.api().columns(1).every(function () {
                                var column = this;
                                $('#seach_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(5).every(function () {
                                var column = this;
                                $('#search_type').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(3).every(function () {
                                var column = this;
                                $('#search_status').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                    ]);

                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'api_name', 'name'=> 'api_gateway.api_name', 'title' => 'API name', 'orderable'=>false],
            ['data'=> 'api_url_schema', 'name'=> 'api_gateway.api_url_schema', 'title' => 'API URL', 'orderable'=>false],
            ['data'=> 'status', 'name'=> 'api_gateway.status', 'title' => 'Status', 'orderable'=>false],
            ['data'=> 'update_at', 'name'=> 'api_gateway.update_at', 'title' => 'Modified date', 'orderable'=>false],
            ['data'=> 'conn_type', 'name'=> 'api_connection.conn_type', 'title' => 'conn_type', 'visible' => false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions', 'orderable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Rest_' . date('YmdHis');
    }

    // public function ajax()
    // {
        // return $this->datatables
            // ->eloquent($this->query())
            // ->addColumn('action', 'audits.audit.datatables.action')
            // ->make(true);
    // }
}
