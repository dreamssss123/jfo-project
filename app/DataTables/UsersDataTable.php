<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
// use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
// use Yajra\DataTables\Html\Editor\Fields;
// use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\User;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($row){

                $perms = \App\Perm::can();

                $html  = '';

                if( $perms[2]->edit )
                    $html .= "<a href='".route('backend.user.edit', $row->id)."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                if( $perms[2]->delete ){
                    $html .= "<form method='post' action='".route('backend.user.destroy', $row->id)."' style='display:inline-block'>";
                    $html .= csrf_field();
                    $html .= "<input type='hidden' name='_method' value='DELETE'>";
                    $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                    $html .= "</form>";
                }

                return $html;
            })
            ->editColumn('status', function($row){
                if( $row->status == 1 )
                    $html = "<span class='stat_success'>Enable</span>";
                else
                    $html = "<span class='stat_danger'>Disable</span>";

                return $html;
            })
            ->editColumn('updated_at', function($row){
                $c = new Carbon;
                $parse = $c->parse($row->updated_at);

                return $parse->format('d/m/Y H:i');
            })
            ->addIndexColumn()
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\UsersDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        // print_r(request()->all());exit;
        $model = $model->newQuery()->select('users.id', \DB::raw('CONCAT(COALESCE(users.firstname, ""), " ", COALESCE(users.lastname, "")) AS full_name'), 'users.email', 'users.status', 'users.updated_at', 'roles.name as role_name')
                                    ->leftJoin('roles', 'users.role_id', 'roles.id')
                                    ->orderBy('users.updated_at', 'desc');

        // if(  isset( request()['order'][0]['column'] ) && request()['order'][0]['column'] == 0 )
        //     $model = $model->orderBy('updated_at', 'desc');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    // ->setTableId('usersdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    // ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'dom'          => 'Bfrtip',
                        // 'bFilter' => false,
                        // 'columnDefs' => [
                        //     [
                        //         "targets" => 5,
                        //         "visible" => false,
                        //         // "searchable": false
                        //     ],
                        // ],
                        'initComplete' => "function () {
                            
                            this.api().columns(1,2).every(function () {
                                var column = this;
                                $('#seach_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(3).every(function () {
                                var column = this;
                                $('#seach_email').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(4).every(function () {
                                var column = this;
                                $('#search_role').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(5).every(function () {
                                var column = this;
                                $('#search_status').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                    ]);
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        // return [
        //     Column::computed('action')
        //           ->exportable(false)
        //           ->printable(false)
        //           ->width(60)
        //           ->addClass('text-center'),
        //     Column::make('id'),
        //     Column::make('add your columns'),
        //     Column::make('created_at'),
        //     Column::make('updated_at'),
        // ];
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'full_name', 'name'=> 'users.firstname', 'title' => 'Contact name', 'orderable'=>false],
            ['data'=> 'full_name', 'name'=> 'users.lastname', 'visible' => false],
            ['data'=> 'email', 'name'=> 'email', 'title' => 'e-Mail', 'orderable'=>false],
            ['data'=> 'role_name', 'name'=> 'roles.name', 'title' => 'Role', 'orderable'=>false],
            ['data'=> 'status', 'name'=> 'status', 'title' => 'Status', 'orderable'=>false],
            ['data'=> 'updated_at', 'name'=> 'updated_at', 'title' => 'Last Update', 'orderable'=>false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions', 'orderable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
