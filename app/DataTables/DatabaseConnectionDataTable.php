<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;
use App\Connection;
use Auth;


class DatabaseConnectionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($row){

                $user = \Auth::user();
                $perms = \App\Perm::can();
            
                $html = '';

                if( $user->role_id == config('app.sa_id') ){
                    $html .= "<a href='".route('backend.dbconn.edit', $row->id)."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                    $html .= "<form method='post' action='".route('backend.dbconn.destroy', $row->id)."' style='display:inline-block'>";
                    $html .= csrf_field();
                    $html .= "<input type='hidden' name='_method' value='DELETE'>";
                    $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                    $html .= "</form>";
                }else{

                    if( $row->user_id == $user->id ){
                        if( $perms[4]->edit )
                            $html .= "<a href='".route('backend.dbconn.edit', $row->id)."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                        if( $perms[4]->delete ){
                            $html .= "<form method='post' action='".route('backend.dbconn.destroy', $row->id)."' style='display:inline-block'>";
                            $html .= csrf_field();
                            $html .= "<input type='hidden' name='_method' value='DELETE'>";
                            $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                            $html .= "</form>";
                        }
                    }
                }
                    
                return $html;
            })
            ->editColumn('conn_status', function($row){
                if( $row->conn_status  == 1 )
                    $html = "<span class='stat_success'>Enable</span>";
                else
                    $html = "<span class='stat_danger'>Disable</span>";

                return $html;
            })
            ->editColumn('update_at', function($row){
                $c = new Carbon;
                $parse = $c->parse($row->update_at);

                return $parse->format('d/m/Y H:i');
            })
            ->addIndexColumn()
            ->rawColumns(['conn_status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\UsersDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Connection $model)
    {
        $user = Auth::user();

        $model = $model->newQuery()->select('id', 'user_id', 'conn_name', 'conn_ip', 'conn_status', 'update_at');

        if( $user->role_id != config('app.sa_id') )
            $model = $model->where('user_id', $user->id);

        if(  isset( request()['order'][0]['column'] ) && request()['order'][0]['column'] == 0 )
            $model = $model->orderBy('updated_at', 'desc');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('usersdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'conn_name', 'name'=> 'conn_name', 'title' => 'Database connection name', 'orderable'=>false],
            ['data'=> 'conn_ip', 'name'=> 'conn_ip', 'title' => 'IP Address', 'orderable'=>false],
            ['data'=> 'conn_status', 'name'=> 'conn_status', 'title' => 'Status', 'orderable'=>false],
            ['data'=> 'update_at', 'name'=> 'update_at', 'title' => 'Modified date', 'orderable'=>false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions', 'orderable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
