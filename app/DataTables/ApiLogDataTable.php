<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\ApiLog;

class ApiLogDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('partner_name', function($row){
                $name = $row->partner_name;
                if( !$name )
                    $name = '-';
                
                return $name;
            })
            ->editColumn('log_type', function($row){
                if( $row->log_type == 'req' )
                    $text = 'Request';
                else
                    $text = 'Response';

                return $text;
            })
            ->editColumn('datetime_at', function($row){
                $c = new Carbon;
                $parse = $c->parse($row->datetime_at);

                return $parse->format('d/m/Y H:i');
            })
            ->editColumn('status_code', function($row){
                if( $row->status_code == '200' )
                    $text = "<span class='stat_code_success'>".$row->status_code."</span>";
                else
                    $text = "<span class='stat_code_error'>".$row->status_code."</span>";

                return $text;
            })
            ->addColumn('action', function($row){
                $html = "<a href='".route('backend.apilog.show', $row->id)."' class='btn btn-info btn-sm'>Detail</a>";
                return $html;
            })
            ->addIndexColumn()
            ->rawColumns(['status_code', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ApiLogDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ApiLog $model)
    {
        // print_r($_GET);
        $columns = request()['columns'];
        $model = $model->newQuery()->select('api_log.id', 'api_log.path', 'api_log.partner_name', 'api_log.status_code', 'api_log.log_type', 'api_log.datetime_at', 'api_log.datetime_at as datetime_normal');

        if( isset($columns[5]['search']) ){
            $data_date = json_decode($columns[5]['search']['value']);
            // print_r($data_date;exit;
            if( $data_date->date_start && $data_date->date_end )
                $model = $model->whereBetween('api_log.datetime_at', [$data_date->date_start, $data_date->date_end]);
            else if( $data_date->date_start && !$data_date->date_end )
                $model = $model->where('api_log.datetime_at', '>', $data_date->date_start);
            else if( !$data_date->date_start && $data_date->date_end )
                $model = $model->where('api_log.datetime_at', '<', $data_date->date_end);
        }

        if( @request()->input('log_type') == 'error' )
            $model = $model->where('api_log.status_code', '!=', '200');

        $model = $model->orderBy('api_log.datetime_at', 'desc');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    // ->setTableId('apilogdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    // ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'dom'          => 'Bfrtip',
                        // 'searching' => false,
                        // 'bFilter' => false,
                        // 'columnDefs' => [
                        //     [
                        //         "targets" => 5,
                        //         "visible" => false,
                        //         // "searchable": false
                        //     ],
                        // ],
                        'initComplete' => "function () {

                            this.api().cells('#search_date_start').data();
                            
                            this.api().columns(1).every(function () {
                                var column = this;
                                $('#seach_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(3).every(function () {
                                var column = this;
                                $('#seach_partner').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });

                            this.api().columns(6).every(function () {
                                var column = this;
                                $('#search_status').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            
                            this.api().columns(5).every(function () {
                                var column = this;

                                $('#search_date_start, #search_date_end').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm',
                                })
                                .on('dp.change', (e)=>{
                                    var two_date = [];
                                    two_date = {
                                        'date_start': $('#search_date_start').val(),
                                        'date_end': $('#search_date_end').val()
                                    };
                                    var data_two_date = JSON.stringify(two_date);
                                    column.search(data_two_date, false, false, true).draw();
                                });

                                $('#search_date_start, #search_date_end').keyup(function(){
                                    var two_date = [];
                                    two_date = {
                                        'date_start': $('#search_date_start').val(),
                                        'date_end': $('#search_date_end').val()
                                    };
                                    var data_two_date = JSON.stringify(two_date);
                                    column.search(data_two_date, false, false, true).draw();
                                });
                            });
                        }",

                        // 'initComplete' => "function () {
                            
                        //     let select1 = '';
                        //     select1 += '<select class=form-control tabindex=\"-1\" aria-hidden=\"true\" style=\"max-width:200px\">';
                        //     select1 +=      '<option>-- Select --</option>';
                        //     select1 +=      '<option value=\"z\">res</option>';
                        //     select1 +=      '<option value=\"req\">req</option>';
                        //     select1 += '</select>';

                        //     this.api().columns([1,2]).indexes().flatten().each(function() {
                        //         var column = this;
                        //         var select = $(select1)
                        //             .appendTo($('#filter_box'))
                        //             .on('change', function() {
                        //                 var val = $.fn.dataTable.util.escapeRegex(
                        //                     $(this).val());
                        //                 column.search(val).draw();
                        //             });
                        //     });
                        // }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'path', 'name'=> 'path', 'title' => 'API URL', 'orderable'=>false],
            ['data'=> 'log_type', 'name'=> 'log_type', 'title' => 'Type', 'orderable'=>false],
            ['data'=> 'partner_name', 'name'=> 'partner_name', 'title' => 'Partner name', 'orderable'=>false],
            // ['data'=> 'log_type', 'name'=> 'log_type', 'title' => 'Partner name'],
            ['data'=> 'datetime_at', 'name'=> 'api_log.datetime_at', 'title' => 'Datetime', 'className' => 'text-center', 'orderable'=>false],
            ['data'=> 'datetime_normal', 'name'=> 'api_log.datetime_at', 'title' => 'Datetime Hide', 'visible' => false, 'searchable' => false],
            ['data'=> 'status_code', 'name'=> 'status_code', 'title' => 'Status', 'className' => 'text-center', 'orderable'=>false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions', 'orderable'=>false, 'className' => 'text-center'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ApiLog_' . date('YmdHis');
    }
}
