<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\ApiLog;

class ReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('api_name', function($row){
                $text = $row->api_name;
                if( !$text )
                    $text = '-';

                return $text;
            })
            ->editColumn('path', function($row){
                return 'api/'.$row->path;
            })
            ->editColumn('partner_name', function($row){
                $text = $row->partner_name;
                if( !$text )
                    $text = '-';

                return $text;
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ReportDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ApiLog $model)
    {
        // return $model->newQuery()->select('api_log.path', 'api_log.partner_name', 'api_gateway.api_name', \DB::raw('count(*) as total'))
        //                         ->leftJoin('api_gateway', 'api_log.path', 'api_gateway.api_url_schema')
        //                         ->where('api_log.log_type', 'req')
        //                         ->groupBy('api_log.path', 'api_log.partner_name', 'api_gateway.api_name')
        //                         ->get();

        $columns = request()['columns'];
        // print_r($columns);exit;
        $model = $model->newQuery()->select('api_log.path', 'api_log.partner_name', 'api_log.api_name', \DB::raw('count(*) as total'))
                                ->where(function($q){
                                    $q->orWhere('api_log.log_type', 'req');
                                    $q->orWhere('api_log.api_name', 'auth_login');
                                });

        if( isset($columns[4]['search']) ){
            $data_date = json_decode($columns[4]['search']['value']);
            // print_r($data_date);exit;
            if( $data_date->date_start && $data_date->date_end )
                $model = $model->whereBetween('api_log.datetime_at', [$data_date->date_start, $data_date->date_end]);
            else if( $data_date->date_start && !$data_date->date_end )
                $model = $model->where('api_log.datetime_at', '>', $data_date->date_start);
            else if( !$data_date->date_start && $data_date->date_end )
                $model = $model->where('api_log.datetime_at', '<', $data_date->date_end);
        }

        $model = $model->groupBy('api_log.path', 'api_log.partner_name', 'api_log.api_name')
                        ->get();

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('reportdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'initComplete' => "function () {

                            this.api().columns(1).every(function () {
                                var column = this;
                                $('#search_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            
                            this.api().columns(3).every(function () {
                                var column = this;
                                $('#search_partner').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            

                            this.api().columns(4).every(function () {
                                var column = this;

                                $('#search_date_start, #search_date_end').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm',
                                })
                                .on('dp.change', (e)=>{
                                    var two_date = [];
                                    two_date = {
                                        'date_start': $('#search_date_start').val(),
                                        'date_end': $('#search_date_end').val()
                                    };
                                    var data_two_date = JSON.stringify(two_date);
                                    //console.log('start_top : '+data_two_date);
                                    column.search(data_two_date, false, false, true).draw();
                                });

                                $('#search_date_start, #search_date_end').keyup(function(){
                                    var two_date = [];
                                    two_date = {
                                        'date_start': $('#search_date_start').val(),
                                        'date_end': $('#search_date_end').val()
                                    };
                                    var data_two_date = JSON.stringify(two_date);
                                    //console.log('start_bot : '+data_two_date);
                                    column.search(data_two_date, false, false, true).draw();
                                });
                            });
                        }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'api_name', 'name'=> 'api_name', 'title' => 'API name', 'orderable'=>false],
            ['data'=> 'path', 'name'=> 'path', 'title' => 'API URL', 'orderable'=>false],
            ['data'=> 'partner_name', 'name'=> 'partner_name', 'title' => 'Partner name', 'orderable'=>false],
            ['data'=> 'total', 'name'=> 'total', 'title' => 'Usage', 'className' => 'text-center', 'orderable'=>false, 'searchable'=> false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Report_' . date('YmdHis');
    }
}
