<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use Carbon\Carbon;
use App\Partner;

class PartnerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($row){

                $perms = \App\Perm::can();

                $html = '';

                if( $perms[3]->edit )
                    $html .= "<a href='".route('backend.partner.edit', $row->id)."' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></a> ";

                if( $perms[3]->delete ){
                    $html .= "<form method='post' action='".route('backend.partner.destroy', $row->id)."' style='display:inline-block'>";
                    $html .= csrf_field();
                    $html .= "<input type='hidden' name='_method' value='DELETE'>";
                    $html .= "<button type='button' class='btn btn-danger btn-sm btn_confirm_warning' data-title='Confirm Delete'><i class='fa fa-trash'></i></button>";
                    $html .= "</form>";
                }

                return $html;
            })
            ->editColumn('status', function($row){
                if( $row->status  == 1 )
                    $html = "<span class='stat_success'>Enable</span>";
                else
                    $html = "<span class='stat_danger'>Disable</span>";

                return $html;
            })
            ->editColumn('update_at', function($row){
                $c = new Carbon;
                $parse = $c->parse($row->update_at);

                return $parse->format('d/m/Y H:i');
            })
            ->addIndexColumn()
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PartnerDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Partner $model)
    {
        return $model->newQuery()->select('api_partner.id', 'api_partner.par_name', 'api_partner.par_person_contact', 'api_partner.par_email', 'api_partner.status', 'api_partner.update_at', 'api_category.cat_name')
                                ->leftJoin('api_category', 'api_partner.category_id', 'api_category.id')
                                ->orderBy('api_partner.update_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    // ->setTableId('partnerdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'initComplete' => "function () {

                            this.api().columns(1).every(function () {
                                var column = this;
                                $('#search_name').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            this.api().columns(2).every(function () {
                                var column = this;
                                $('#search_contact').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            this.api().columns(3).every(function () {
                                var column = this;
                                $('#search_email').on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            this.api().columns(4).every(function () {
                                var column = this;
                                $('#search_cate').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                            this.api().columns(5).every(function () {
                                var column = this;
                                $('#search_status').on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=> 'DT_RowIndex', 'title' => '#', 'orderable'=> false, 'searchable'=> false],
            ['data'=> 'par_name', 'name'=> 'par_name', 'title' => 'Partner name', 'orderable'=>false],
            ['data'=> 'par_person_contact', 'name'=> 'par_person_contact', 'title' => 'Contact name', 'orderable'=>false],
            ['data'=> 'par_email', 'name'=> 'par_email', 'title' => 'Email', 'orderable'=>false],
            ['data'=> 'cat_name', 'name'=> 'api_category.cat_name', 'visible' => false],
            ['data'=> 'status', 'name'=> 'status', 'title' => 'Status', 'orderable'=>false],
            ['data'=> 'update_at', 'name'=> 'update_at', 'title' => 'Modified date', 'orderable'=>false],
            ['data'=> 'action', 'name'=> 'action', 'title' => 'Actions', 'orderable'=>false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Partner_' . date('YmdHis');
    }
}
