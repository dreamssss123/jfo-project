<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'status'];

    public function perms()
    {
    	return $this->hasMany('App\Perm', 'role_id', 'id');
    }
}
