<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model
{
    protected $table = 'system_log';

    protected $fillable = ['status_code', 'ip', 'path', 'agent', 'partner_name', 'datetime_at', 'methods', 'txt_data', 'log_type', 'ref_id', 'user_id', 'status'];

    public $timestamps = false;



    static function save_log($log)
    {
    	$c = new \Carbon\Carbon;
    	$now = $c->now();

    	$last = self::select('id', 'status_code', 'path', 'log_type', 'user_id', 'datetime_at')->orderBy('id', 'desc')->first();

    	if( $last && $last->status_code == $log['code'] && $last->path == $log['path'] && $last->log_type == $log['type'] && $last->user_id == $log['user_id'] ){

    		$last_parse = $c->parse($last->datetime_at);
    		if( $now->diffInSeconds($last_parse) <= 2 )
    			return false;
    	}

    	$save = self::create([
    		'status_code' => $log['code'],
    		'ip' => $_SERVER['REMOTE_ADDR'],
    		'log_type' => $log['type'],
    		'path' => $log['path'],
    		'ref_id' => @$log['ref_id'],
    		'datetime_at' => $now,
    		'user_id' => $log['user_id'],
            'txt_data' => @$log['txt_data'],
    	]);
    }
}
