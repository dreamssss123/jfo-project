<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Partner extends Authenticatable implements JWTSubject
{
    protected $table = 'api_partner';

    protected $fillable = ['par_name', 'category_id', 'par_person_contact', 'par_email', 'username', 'password', 'par_department', 'par_mobile', 'par_address', 'status'];

    const CREATED_AT = 'create_at';
	const UPDATED_AT = 'update_at';

	protected $hidden = [
        'password'
    ];


	public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function api_map()
    {
        return $this->hasMany('App\ApiPartnerMap', 'partner_id', 'id');
    }

    public function partner_org()
    {
        return $this->hasOne('App\ApiCategory', 'id', 'category_id');
    }
}
