<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDelete extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'email', 'organization', 'position', 'phone', 'address', 'role_id', 'password', 'create_at', 'update_at', 'deleted_at',
    ];
}
