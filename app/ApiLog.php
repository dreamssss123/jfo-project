<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiLog extends Model
{
    protected $table = 'api_log';

    protected $fillable = ['status_code', 'ip', 'path', 'api_name', 'agent', 'partner_name', 'datetime_at', 'methods', 'txt_data', 'log_type', 'status'];

    public $timestamps = false;
}
