<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
	protected $table = 'api_configuration';

    protected $fillable = ['name', 'description', 'global_quantity', 'global_time_alert', 'global_page_limit', 'global_mail_error', 'create_at', 'update_at', 'status'];

    const CREATED_AT = 'create_at';
	const UPDATED_AT = 'update_at';
}
