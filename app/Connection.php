<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $table = 'api_connection';

    protected $fillable = ['user_id', 'conn_type', 'conn_name', 'conn_alias', 'conn_ip', 'username', 'password', 'conn_port', 'conn_sid', 'conn_database', 'conn_status'];

    const CREATED_AT = 'create_at';
	const UPDATED_AT = 'update_at';




	static function validate($type, $connection_type)
    {
        if( $type == 'create' ){
            $validate = [
                'conn_type' => 'required',
                'conn_name' => 'required',
                'conn_alias' => 'required|unique:api_connection|min:6',
                'conn_ip' => 'required',
                'username' => 'required',
                // 'password' => 'required',
            ];

        }else{
        	$validate = [
                'conn_type' => 'required',
                'conn_name' => 'required',
                'conn_ip' => 'required',
                'username' => 'required',
            ];
        }

        if( $connection_type != 'oracle' ){
            $validate['conn_database'] = 'required';
        }else{
            $validate['conn_sid'] = 'required';
        }


        $msg = [
        	'conn_name.required' => 'The Database connection name field is required.',
        	'conn_alias.required' => 'The Alias field is required.',
        	'conn_alias.unique' => 'The Alias field has already been taken.',
        	'conn_alias.min' => 'The Alias field must be at least 6 characters.',
        	'conn_ip.required' => 'The IP Address field is required.',
        	'username.required' => 'The IP Username field is required.',
        	'password.required' => 'The Password field is required.',
        	'conn_database.required' => 'The Database name field is required.',
        	'sid.required' => 'The Service Name/SID field is required.',
        ];

        $valid = [
        	'valid' => $validate,
        	'msg' => $msg,
        ];

        return $valid;
    }
}
