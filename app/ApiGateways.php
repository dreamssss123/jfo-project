<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiGateways extends Model
{
	protected $table = 'api_gateway';

    protected $fillable = ['api_name', 'api_type', 'api_url_schema', 'api_description', 'schema_req_markup', 'schema_res_markup', 'schema_req_desc', 'schema_res_desc', 'conn_alias', 'conn_table', 'status'];

    const CREATED_AT = 'create_at';
	const UPDATED_AT = 'update_at';

	public function connection()
	{
		return $this->hasOne('App\Connection', 'conn_alias', 'conn_alias');
	}

	public function partner_map()
	{
		return $this->hasOne('App\ApiPartnerMap', 'api_id', 'id');
	}

	public function partner_maps()
	{
		return $this->hasMany('App\ApiPartnerMap', 'api_id', 'id');
	}
}
