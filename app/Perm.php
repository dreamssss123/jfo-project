<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perm extends Model
{
	protected $table = 'permissions';

    protected $fillable = ['role_id', 'menu_id', 'can_view', 'can_create', 'can_edit', 'can_delete'];

    public function menu()
    {
    	return $this->hasOne('App\Menu', 'id', 'menu_id');
    }

    static function can()
    {
    	return session()->get('perms');
    }
}
