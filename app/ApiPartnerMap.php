<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiPartnerMap extends Model
{
    protected $table = 'api_partner_mapping';

    protected $fillable = ['api_id', 'partner_id'];

    public $timestamps = false;

    public function api()
    {
    	return $this->hasOne('App\ApiGateways', 'id', 'api_id');
    }
}
