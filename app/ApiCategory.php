<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiCategory extends Model
{
    protected $table = 'api_category';

    protected $fillable = ['cat_name', 'status'];

    const CREATED_AT = 'create_at';
	const UPDATED_AT = 'update_at';

	public function partner()
	{
		return $this->hasMany('App\Partner', 'category_id', 'id');
	}
}
