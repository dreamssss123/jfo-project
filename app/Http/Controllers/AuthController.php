<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\Partner;

class AuthController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
        $this->middleware('api_auth', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
    	$credentials = request(['username', 'password']);

        if (! $token = auth('api')->setTTL(30)->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    	// echo $pn['password'];exit;
    	// $partner = Partner::where('username', $pn['username'])->first();

    	// if( !$partner || !decrypt($partner->password) == $pn['password'] )
    	// 	return response()->json(['error' => 'Unauthorized'], 401);

    	// \Config::set('auth.defaults.guard', 'api');
    	// $r = config('auth.defaults.guard');

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
	{
		// print_r(JWTAuth::factory()->getTTL());exit;
		// try{
		// 	JWTAuth::parseToken()->validate();
		// }catch(Exception $e){
		// 	echo 555;
		// }
		// exit;

	 //    try {
	 //        if (! $user = JWTAuth::parseToken()->authenticate()) {

	 //            return response()->json(['user_not_found'], 404);
	 //        }

	 //    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

	 //        $refreshed = JWTAuth::refresh(JWTAuth::getToken());
	 //        $user = JWTAuth::setToken($refreshed)->toUser();
	 //        return $this->response->withArray(['token' => $refreshed]);
	 //    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

	 //        return response()->json(['token_invalid'], $e->getStatusCode());

	 //    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

	 //        return response()->json(['token_absent'], $e->getStatusCode());

	 //    }

	 //    return $user;
  //   	return '565465465';
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
