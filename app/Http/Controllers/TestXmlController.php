<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\User;

class TestXmlController extends Controller
{
    public function index()
    {
    	// return view('test_xml');

        error_reporting(E_ALL);
        ini_set('display_errors', 'On');

        $conn = oci_connect('system', '1234', 'orc2l');
        print_r($conn);

        // $stid = oci_parse($conn, 'select table_name from user_tables');
        // oci_execute($stid);

        // echo "<table>\n";
        // while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        //     echo "<tr>\n";
        //     foreach ($row as $item) {
        //         echo "  <td>".($item !== null ? htmlspecialchars($item, ENT_QUOTES) : "&nbsp;")."</td>\n";
        //     }
        //     echo "</tr>\n";
        // }
        // echo "</table>\n";
    }

    public function test_export_word()
    {

    	$phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        // Adding Text element to the Section having font styled by default...
        $section->addText(
        '"Learn from yesterday, live for today, hope for tomorrow. '
        . 'The important thing is not to stop questioning." '
        . '(Albert Einstein)'
        );

        $section->addText('"Great achievement is usually born of great sacrifice, '. 'and is never the result of selfishness." '. '(Napoleon Hill)', array('name' => 'Tahoma', 'size' => 10));

        $fontStyleName = 'oneUserDefinedStyle';
        $phpWord->addFontStyle(
        $fontStyleName,
        array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
        );
        $section->addText(
        '"The greatest accomplishment is not in never falling, '
        . 'but in rising again after you fall." '
        . '(Vince Lombardiasdsada)',
        $fontStyleName
        );

        $tableStyle = array(
            'borderColor' => '000',
            'borderSize' => 6,
            'cellMargin' => 50
        );
        // $firstRowStyle = array('bgColor' => '66BBFF');
        // $phpWord->addTableStyle('myTable', $tableStyle, $firstRowStyle);

        $table = $section->addTable($tableStyle);
        for ($r = 1; $r <= 50; $r++) {
            $table->addRow();
            for ($c = 1; $c <= 5; $c++) {
                $table->addCell(1750)->addText("Row {$r}, Cell {$c}");
            }
        }


        // $table = $section->addTable([$tableStyle]);
        // $table->addRow([$height], [$rowStyle]);
        // $cell = $table->addCell($width, [$cellStyle]);


        // $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        // $fontStyle->setBold(true);
        // $fontStyle->setName('Tahoma');
        // $fontStyle->setSize(13);
        // $myTextElement = $section->addText('"Believe you can and you\'re halfway there."
        // ˓→(Theodor Roosevelt)');
        // $myTextElement->setFontStyle($fontStyle);

        // Saving the document as OOXML file...
        $file = 'doc-'.date('YmdHis').'.docx';
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('docs/'.$file);
        // echo asset($file);exit;

        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $file . '"');
        readfile(asset('docs/'.$file));
        // unlink(asset($file));
        // header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.
        // ˓→document');
        // header('Content-Transfer-Encoding: binary');
        // header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        // header('Expires: 0');


    }

    public function test_send_mail()
    {
        // echo 555;exit;
        $to_name = 'OFza123';
        $to_email = 'ofaion123@gmail.com';

        Mail::send('emails.alert', [], function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Hi OFza123');
            $message->from('lnwza555@mail.com');
        });
    }
}
