<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ApiLog;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        // phpinfo();exit;
        $syslog = [
            'code' => 200,
            'type' => 'view',
            'path' => 'dashboard',
            'user_id' => Auth::user()->id,
        ];
        \App\SystemLog::save_log($syslog);

    	$log_pops = ApiLog::select('api_log.path', \DB::raw('count(*) as total'), 'api_log.api_name')
                        ->where('api_log.log_type', 'req')
    					->orderBy('total', 'desc')
                        ->groupBy('api_log.path', 'api_log.api_name')
    					->limit(5)
    					->get();
    	// return $log_pop;
        $log_errors = ApiLog::select('api_log.id', 'api_log.path', 'api_log.datetime_at', 'api_log.api_name')
                        ->where('api_log.log_type', 'res')
                        ->where('api_log.status_code', '!=', '200')
                        // ->groupBy('path')
                        ->orderBy('api_log.datetime_at', 'desc')
                        ->limit(5)
                        ->get();
        // return $log_errors;
        $report = ApiLog::select('api_log.path', 'api_log.api_name', \DB::raw('count(*) as total'))
                        ->where('api_log.log_type', 'req')
                        ->where('api_log.path', '!=', 'auth/login')
                        ->groupBy('api_log.path', 'api_log.api_name')
                        ->limit(20)
                        ->get();
        // return $report;

    	return view('backend.index', compact('log_pops', 'log_errors', 'report'));
    }
}
