<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\DataTables\UsersDataTable;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[2]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'user',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");

        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'user',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perms = \App\Perm::can();
        if( !$perms[2]->create ){

            $syslog = [
                'code' => 401,
                'type' => 'insert',
                'path' => 'user',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return view('backend.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // dd($request->all());
        $user = new User;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->organization = $request->organization;
        $user->position = $request->position;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->status = $request->status;
        $user->role_id = $request->role_id;
        $user->password = Hash::make($request->password);
        $user->save();

        $syslog = [
            'code' => 200,
            'type' => 'insert',
            'path' => 'user',
            'ref_id' => $user->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Create user : '.$user->email,
        ];
        \App\SystemLog::save_log($syslog);

        return redirect()->route('backend.user.edit', $user->id)->with('message', 'Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perms = \App\Perm::can();
        if( !$perms[2]->edit ){

            $syslog = [
                'code' => 401,
                'type' => 'update',
                'path' => 'user',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        $user = User::findOrFail($id);
        // return $user;
        return view('backend.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        // dd($request->all());
        $u = new User;
        // echo $u->blank_password;exit;
        $user = User::findOrFail($id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->organization = $request->organization;
        $user->position = $request->position;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->status = $request->status;
        $user->role_id = $request->role_id;

        if( $request->password != $u->blank_password )
            $user->password = Hash::make($request->password);

        $user->save();

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'user',
            'ref_id' => $user->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update user : '.$user->email,
        ];
        \App\SystemLog::save_log($syslog);
    
        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $syslog = [
            'code' => 200,
            'type' => 'delete',
            'path' => 'user',
            'ref_id' => $user->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Delete user : '.$user->email,
        ];
        \App\SystemLog::save_log($syslog);
        
        // return $user;
        $del = \App\UserDelete::create([
            'firstname' => $user->firstname, 
            'lastname' => $user->lastname, 
            'email' => $user->email, 
            'organization' => $user->organization, 
            'position' => $user->position, 
            'phone' => $user->phone, 
            'address' => $user->address, 
            'role_id' => $user->role_id, 
            'password' => $user->password, 
            'create_at' => $user->created_at, 
            'update_at' => $user->updated_at, 
            'deleted_at' => Carbon::now(),
        ]);

        if( $del )
            $user->delete();

        return redirect()->route('backend.user.index')->with('message', 'Deleted');
    }
}
