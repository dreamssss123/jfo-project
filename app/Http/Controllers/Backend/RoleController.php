<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Role;
use App\Perm;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perms = \App\Perm::can();
        if( !$perms[11]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'role',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");

        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'role',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        $roles = Role::orderBy('created_at', 'desc')->get();

        return view('backend.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perms = \App\Perm::can();
        if( !$perms[11]->create ){

            $syslog = [
                'code' => 401,
                'type' => 'insert',
                'path' => 'role',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'name' => 'required|unique:roles',
        ]);
        
        $role = Role::create([
            'name' => $request->name,
            'status' => $request->status,
        ]);

        $syslog = [
            'code' => 200,
            'type' => 'insert',
            'path' => 'role',
            'ref_id' => $role->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Create role : '.$role->name,
        ];
        \App\SystemLog::save_log($syslog);

        // return $role;
        $menus = Menu::orderBy('sort', 'asc')->get();
        foreach( $menus as $menu ){
            $data = [
                'role_id' => $role->id,
                'menu_id' => $menu->id,
                'can_view' => 0,
                'can_create' => 0,
                'can_edit' => 0,
                'can_delete' => 0,
            ];
            if( isset($request->can_view[$menu->id]) )
                $data['can_view'] = 1;
            if( isset($request->can_create[$menu->id]) )
                $data['can_create'] = 1;
            if( isset($request->can_edit[$menu->id]) )
                $data['can_edit'] = 1;
            if( isset($request->can_delete[$menu->id]) )
                $data['can_delete'] = 1;
            
            $perm = Perm::create($data);
        }

        return redirect()->route('backend.role.edit', $role->id)->with('message', 'Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perms = \App\Perm::can();
        if( !$perms[11]->edit ){

            $syslog = [
                'code' => 401,
                'type' => 'update',
                'path' => 'role',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        $role = Role::findOrFail($id);
        $role_perm = $role->perms;

        return view('backend.role.edit', compact('role', 'role_perm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $role = Role::findOrFail($id);
        $role->update([
            'status' => $request->status,
        ]);

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'role',
            'ref_id' => $role->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update role : '.$role->name,
        ];
        \App\SystemLog::save_log($syslog);

        $role_perm = $role->perms;

        $p = new \App\Perm;
        $menus = Menu::orderBy('sort', 'asc')->get();
        foreach( $menus as $menu ){

            $can_view = 0;
            if( isset($request->can_view[$menu->id]) )
                $can_view = 1;

            $can_create = 0;
            if( isset($request->can_create[$menu->id]) )
                $can_create = 1;
            
            $can_edit = 0;
            if( isset($request->can_edit[$menu->id]) )
                $can_edit = 1;

            $can_delete = 0;
            if( isset($request->can_delete[$menu->id]) )
                $can_delete = 1;

            $can = $p->where('role_id', $role->id)
                    ->where('menu_id', $menu->id)
                    ->first();

            if( $can )
                $can->update([
                    'can_view' => $can_view,
                    'can_create' => $can_create,
                    'can_edit' => $can_edit,
                    'can_delete' => $can_delete,
                ]);
            else
                $p = Perm::create([
                    'role_id' => $role->id,
                    'menu_id' => $menu->id,
                    'can_view' => $can_view,
                    'can_create' => $can_create,
                    'can_edit' => $can_edit,
                    'can_delete' => $can_delete,
                ]);
        }

        // foreach( $role_perm as $perm ){

        //     if( isset($request->can_view[$perm->id]) )
        //         $perm->can_view = 1;
        //     else
        //         $perm->can_view = 0;

        //     if( isset($request->can_create[$perm->id]) )
        //         $perm->can_create = 1;
        //     else
        //         $perm->can_create = 0;

        //     if( isset($request->can_edit[$perm->id]) )
        //         $perm->can_edit = 1;
        //     else
        //         $perm->can_edit = 0;

        //     if( isset($request->can_delete[$perm->id]) )
        //         $perm->can_delete = 1;
        //     else
        //         $perm->can_delete = 0;

        //     $perm->save();
        // }

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $syslog = [
            'code' => 200,
            'type' => 'delete',
            'path' => 'role',
            'ref_id' => $role->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Delete role : '.$role->name,
        ];
        \App\SystemLog::save_log($syslog);

        Perm::where('role_id', $role->id)->delete();
        $role->delete();

        return redirect()->route('backend.role.index')->with('message', 'Deleted');
    }
}
