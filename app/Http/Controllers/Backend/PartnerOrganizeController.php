<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\PartnerOrganizeDataTable;
use App\ApiCategory;
use Auth;

class PartnerOrganizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PartnerOrganizeDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[9]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'partner_organization',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'partner_organization',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.partner_org.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perms = \App\Perm::can();
        if( !$perms[9]->create ){

            $syslog = [
                'code' => 401,
                'type' => 'insert',
                'path' => 'partner_organization',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return view('backend.partner_org.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'cat_name' => 'required|unique:api_category',
        ]);

        $cat = new ApiCategory;
        $cat->cat_name = $request->cat_name;
        $cat->status = $request->status;
        $cat->save();

        $syslog = [
            'code' => 200,
            'type' => 'insert',
            'path' => 'partner_organization',
            'ref_id' => $cat->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Create partner organization : '.$cat->cat_name,
        ];
        \App\SystemLog::save_log($syslog);

        return redirect()->route('backend.partner_org.index')->with('message', 'Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perms = \App\Perm::can();
        if( !$perms[9]->edit ){

            $syslog = [
                'code' => 401,
                'type' => 'update',
                'path' => 'partner_organization',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        $cat = ApiCategory::findOrFail($id);

        return view('backend.partner_org.edit', compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat = ApiCategory::findOrFail($id);
        $cat->cat_name = $request->cat_name;
        $cat->status = $request->status;
        $cat->save();

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'partner_organization',
            'ref_id' => $cat->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update partner organization : '.$cat->cat_name,
        ];
        \App\SystemLog::save_log($syslog);

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = ApiCategory::findOrFail($id);

        $syslog = [
            'code' => 200,
            'type' => 'delete',
            'path' => 'partner_organization',
            'ref_id' => $cat->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Delete partner organization : '.$cat->cat_name,
        ];
        \App\SystemLog::save_log($syslog);

        foreach( $cat->partner as $row ){
            $row->delete();
        }

        $cat->delete();

        return redirect()->route('backend.partner_org.index')->with('message', 'Deleted');
    }
}
