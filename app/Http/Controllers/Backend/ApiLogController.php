<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\ApiLogDataTable;
use App\ApiLog;
use Auth;

class ApiLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApiLogDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[7]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'apilog',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'apilog',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.log.api_log');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = ApiLog::findOrFail($id);

        return view('backend.log.api_list_schema', compact('log'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export_data(Request $request)
    {
        // print_r($request->all());exit;
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $logs = ApiLog::select('api_log.id', 'api_log.ip', 'api_log.path', 'api_log.partner_name', 'api_log.agent', 'api_log.status_code', 'api_log.log_type', 'api_log.datetime_at', 'api_log.datetime_at as datetime_normal');

        if( $request->seach_name )
            $logs = $logs->where('api_log.path', 'LIKE', '%'.$request->seach_name.'%');
        if( $request->seach_partner )
            $logs = $logs->where('api_log.partner_name', 'LIKE', '%'.$request->seach_partner.'%');


        if( $request->not_status_200 == 1 )
            $logs = $logs->where('api_log.status_code', '!=', '200');

        if( $request->search_status )
            $logs = $logs->where('api_log.status_code', $request->search_status);

        if( $request->search_date_start && $request->search_date_end )
            $logs = $logs->whereBetween('api_log.datetime_at', [$request->search_date_start, $request->search_date_end]);
        else if( $request->search_date_start && !$request->search_date_end )
            $logs = $logs->where('api_log.datetime_at', '>', $request->search_date_start);
        else if( !$request->search_date_start && $request->search_date_end )
            $logs = $logs->where('api_log.datetime_at', '<', $request->search_date_end);


        $logs = $logs->orderBy('api_log.datetime_at', 'desc')
                    ->get();

        $columns = array('datetime', 'status code', 'ip', 'api', 'agent', 'partner', 'type');

        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($logs as $log) {
            fputcsv($file, array($log->datetime_at, $log->status_code, $log->ip, $log->path, $log->agent, $log->partner_name, $log->log_type));
        }
        exit();
    }
}
