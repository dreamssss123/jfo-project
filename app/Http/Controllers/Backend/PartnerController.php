<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\DataTables\PartnerDataTable;
use App\DataTables\RestPartnerDataTable;
use App\Partner;
use App\ApiGateways;
use App\ApiPartnerMap;
use Auth;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PartnerDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[3]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'partner',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'partner',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.partner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RestPartnerDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[3]->create ){

            $syslog = [
                'code' => 401,
                'type' => 'insert',
                'path' => 'partner',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return $dataTable->render('backend.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($request->all());exit;
        //**** Validate ****//
        $validator = Validator::make($request->all(), [
            'par_name' => 'required',
            'category_id' => 'required',
            'par_person_contact' => 'required',
            'par_email' => 'required|email|regex:/(.+)@(.+)\.(.+)/i',
            'username' => 'required|unique:api_partner|max:45',
            'password' => 'required|min:6',
            'par_mobile' => 'required|max:45',
            'par_address' => 'max:255',
        ],[
            'par_name.required' => 'The Partner name field is required.',
            'par_person_contact.required' => 'The Partner organization field is required.',
            'par_email.required' => 'The email field is required.',
            'par_mobile.required' => 'The Mobile / Tel field is required.',
        ]);

        if( $validator->fails() )
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors(),
            ]);
        //****** End Validate *******//

        $partner = new Partner;
        $partner->par_name = $request->par_name;
        $partner->category_id = $request->category_id;
        $partner->par_person_contact = $request->par_person_contact;
        $partner->par_email = $request->par_email;
        $partner->username = $request->username;
        $partner->password = Hash::make($request->password);
        $partner->par_department = $request->par_department;
        $partner->par_mobile = $request->par_mobile;
        $partner->par_address = $request->par_address;
        $partner->status = $request->status;
        $partner->save();

        if( isset($request->api_id) ){
            foreach( $request->api_id as $row ){
                ApiPartnerMap::create([
                    'partner_id' => $partner->id,
                    'api_id' => $row,
                ]);
            }
        }


        $syslog = [
            'code' => 200,
            'type' => 'insert',
            'path' => 'partner',
            'ref_id' => $partner->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Create Partner : '.$partner->par_name,
        ];
        \App\SystemLog::save_log($syslog);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RestPartnerDataTable $dataTable, $id)
    {
        $perms = \App\Perm::can();
        if( !$perms[3]->edit ){

            $syslog = [
                'code' => 401,
                'type' => 'update',
                'path' => 'partner',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        $partner = Partner::findOrFail($id);

        return $dataTable->render('backend.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // print_r($request->all());exit;
        //**** Validate ****//
        $validator = Validator::make($request->all(), [
            'par_name' => 'required',
            'category_id' => 'required',
            'par_person_contact' => 'required',
            'par_email' => 'required|email|regex:/(.+)@(.+)\.(.+)/i',
            // 'username' => 'required|max:45',
            'password' => 'min:6',
            'par_mobile' => 'required|max:45',
            'par_address' => 'max:255',
        ],[
            'par_name.required' => 'The Partner name field is required.',
            'par_person_contact.required' => 'The Partner organization field is required.',
            'par_email.required' => 'The email field is required.',
            'par_mobile.required' => 'The Mobile / Tel field is required.',
        ]);

        if( $validator->fails() )
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors(),
            ]);
        //****** End Validate *******//

        $u = new \App\User;

        $partner = Partner::findOrFail($id);
        $partner->par_name = $request->par_name;
        $partner->category_id = $request->category_id;
        $partner->par_person_contact = $request->par_person_contact;
        $partner->par_email = $request->par_email;

        if( $request->password != $u->blank_password )
            $partner->password = Hash::make($request->password);

        $partner->par_department = $request->par_department;
        $partner->par_mobile = $request->par_mobile;
        $partner->par_address = $request->par_address;
        $partner->status = $request->status;
        $partner->save();

        if( isset($request->api_id) ){
            $new_id = [];
            foreach( $request->api_id as $row ){
                array_push($new_id, $row);
            }

            $old_id = [];
            foreach( $partner->api_map as $row ){
                if( !in_array($row->api_id, $new_id) )
                    ApiPartnerMap::where('partner_id', $partner->id)
                                ->where('api_id', $row->api_id)
                                ->delete();
                else
                    array_push($old_id, $row->api_id);
            }

            // print_r($old_id);
            foreach( $new_id as $row ){
                if( !in_array($row, $old_id) )
                    ApiPartnerMap::create([
                        'partner_id' => $partner->id,
                        'api_id' => $row,
                    ]);
            }
        }else{
            ApiPartnerMap::where('partner_id', $partner->id)->delete();
        }

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'partner',
            'ref_id' => $partner->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update Partner : '.$partner->par_name,
        ];
        \App\SystemLog::save_log($syslog);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::findOrFail($id);

        $syslog = [
            'code' => 200,
            'type' => 'delete',
            'path' => 'partner',
            'ref_id' => $partner->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Delete Partner : '.$partner->par_name,
        ];
        \App\SystemLog::save_log($syslog);

        $partner->delete();

        return redirect()->route('backend.partner.index')->with('message', 'Deleted');
    }

    public function list_api(Request $request, $partner_id)
    {
        // print_r($request->all());exit;
        // echo $partner_id;exit;
        $data = ApiGateways::select('api_gateway.id', 'api_gateway.api_name', 'api_partner_mapping.api_id', 'api_partner_mapping.partner_id')
                            ->leftJoin('api_partner_mapping', 'api_gateway.id', 'api_partner_mapping.api_id')
                            ->where('api_gateway.api_name', 'LIKE', '%'.$request['query'].'%')
                            // ->where('api_partner_mapping.partner_id', '!=', $partner_id)
                            // ->orWhere('api_partner_mapping.partner_id', '=', '')
                            // ->orWhereNull('api_partner_mapping.partner_id')

                            // ->orWhere('api_partner_mapping.partner_id', '=', '')
                            ->orWhere(function($q){
                                $q->orWhere('api_partner_mapping.partner_id', '=', '');
                                $q->orWhereNull('api_partner_mapping.partner_id');
                            })
                            
                            ->get();


        // $data = ApiGateways::select('id', 'api_name')
        //                     ->where('api_name', 'LIKE', '%'.$request['query'].'%')
        //                     ->where('status', 1)
        //                     ->get();
        // foreach($data as $key => $api){

        //     foreach( $api->partner_maps as $map ){
        //         if( $map->partner_id == $partner_id )
        //             unset($data[$key]);
        //     }
        // }
        // exit;
        // return $data;
        return response()->json($data);
    }
}
