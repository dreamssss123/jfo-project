<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\RestDataTable;
use Illuminate\Support\Facades\Validator;
use App\ApiGateways;
use App\Connection;
use Auth;

class RestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RestDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[5]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'api',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'api',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.rest.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perms = \App\Perm::can();
        if( !$perms[5]->create ){

            $syslog = [
                'code' => 401,
                'type' => 'insert',
                'path' => 'api',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return view('backend.rest.form1');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($request->all());exit;
        //**** Validate ****//
        $validator = Validator::make($request->all(), [
            'api_name' => 'required',
            'api_url_schema' => 'required|min:6|unique:api_gateway',
        ]);

        if( $validator->fails() )
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors(),
            ]);
        //****** End Validate *******//

        $rest = new ApiGateways;
        $rest->api_name = $request->api_name;
        $rest->api_type = $request->api_type;
        $rest->api_url_schema = $request->api_url_schema;
        $rest->api_description = $request->api_description;
        $rest->conn_alias = $request->conn_alias;
        $rest->conn_table = $request->conn_table;
        $rest->status = $request->status;
        $rest->save();

        $syslog = [
            'code' => 200,
            'type' => 'insert',
            'path' => 'api',
            'ref_id' => $rest->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Create api : '.$rest->api_name,
        ];
        \App\SystemLog::save_log($syslog);

        return response()->json([
            'status' => 'success',
            'url' => route('backend.rest.step', [$rest->id, 3]),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function step($id, $step)
    {
        $perms = \App\Perm::can();
        if( !$perms[5]->edit ){

            $syslog = [
                'code' => 401,
                'type' => 'update',
                'path' => 'api',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        $user = Auth::user();
        $rest = ApiGateways::findOrFail($id);
        $conn = $rest->connection;
        // return $conn;

        if( $user->role_id != config('app.sa_id') ){
            // echo $conn->user_id;exit;
            if( !$conn || $conn->user_id != $user->id ){
                $syslog = [
                    'code' => 401,
                    'type' => 'update',
                    'path' => 'api',
                    'user_id' => Auth::user()->id,
                ];
                \App\SystemLog::save_log($syslog);

                return redirect()->route('backend.rest.index')->with('warning', "This is not your API.");
            }
        }


        $dbconn = '';
        $columns = [];

        
        if( $step == 1 || $step == 2 ){
            $dbconn = Connection::where('conn_alias', $rest->conn_alias)->first();
            $view = 'form1';
        }else{

            $dbconn = new \App\Http\Controllers\Backend\DatabaseConnectionController;
            $request = new \Illuminate\Http\Request();
            $request->replace([
                'driver' => $conn->conn_type,
                'conn_ip' => $conn->conn_ip,
                'username' => $conn->username,
                'password' => decrypt($conn->password),
                'database' => $conn->conn_database,
                'port' => $conn->conn_port,
                'conn_sid' => $conn->conn_sid,
            ]);
            $connect = $dbconn->lets_connect($request);
            if( $connect['status'] == 'error' )
                return $connect['msg'];

            $columns = $connect['conn']->getSchemaBuilder()->getColumnListing($rest->conn_table);

            if( $step == 3 )
                $view = 'form3';
            else if( $step == 4 )
                $view = 'form4';
            else if( $step == 5 ){
                // echo public_path('docs/');exit;
                // $files = \File::files(public_path('docs/')); // get all file names
                // foreach( $files as $fi ){
                //     $file2 = pathinfo($fi);
                //     // unlink(public_path('docs/'.$file2['basename']));
                // }
                $del_file = public_path('docs/Api-Doc-'.$rest->id.'.docx');
                if( file_exists($del_file) )
                    unlink($del_file);
                
                $this->create_pdf($rest);
                $view = 'form5';
            }
        }

        return view('backend.rest.'.$view, compact('rest', 'step', 'dbconn', 'columns'));
    }

    public function update_firststep(Request $request, $id)
    {   
        // print_r($request->all());exit;
        //**** Validate ****//
        $validator = Validator::make($request->all(), [
            'api_name' => 'required',
        ]);

        if( $validator->fails() )
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors(),
            ]);
        //****** End Validate *******//

        $rest = ApiGateways::findOrFail($id);
        $rest->api_name = $request->api_name;
        $rest->api_type = $request->api_type;
        $rest->api_description = $request->api_description;
        $rest->schema_req_markup = '';
        $rest->schema_res_markup = '';
        $rest->conn_alias = $request->conn_alias;
        $rest->conn_table = $request->conn_table;
        $rest->status = $request->status;
        $rest->save();

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'api',
            'ref_id' => $rest->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update api : '.$rest->api_name.' step 1 & 2',
        ];
        \App\SystemLog::save_log($syslog);

        return response()->json([
            'status' => 'success',
            'url' => route('backend.rest.step', [$rest->id, 3]),
        ]);
    }

    public function step3_save(Request $request, $id)
    {
        // print_r($request->all());exit;
        $datas = [];
        foreach( $request->cl_name as $key => $column ){

            $rename = $request->rename[$column] ? $request->rename[$column] : $column;
            if( $rename == 'page' )
                $rename = 'page_column';

            if( !isset($request->active[$column]) ){
                $rename = '';
                $active = 0;
            }else{
                $active = 1;
            }
            
            $datas[$column] = [
                'active' => $active,
                'name' => $column,
                'rename' => $rename,
                'operator' => isset($request->operator[$column]) ? $request->operator[$column] : '',
                'desc' => $request->desc[$column],
            ];
        }

        $config = [
            'condition' => $request->condition,
            'configs' => $datas,
        ];
        // print_r($config);exit;

        $rest = ApiGateways::findOrFail($id);
        // return $rest;
        $rest->update(['schema_req_markup' => json_encode($config)]);

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'api',
            'ref_id' => $rest->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update api : '.$rest->api_name.' step 3',
        ];
        \App\SystemLog::save_log($syslog);

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function step4_save(Request $request, $id)
    {
        // print_r($request->all());exit;
        $config = [];
        foreach( $request->cl_name as $key => $column ){

            $rename = $request->rename[$column] ? $request->rename[$column] : $column;

            if( !isset($request->active[$column]) ){
                $rename = '';
                $active = 0;
            }else{
                $active = 1;
            }
            
            $config[$column] = [
                'active' => $active,
                'name' => $column,
                'rename' => $rename,
                'desc' => $request->desc[$column],
            ];
        }
        // print_r($config);exit;

        $rest = ApiGateways::findOrFail($id);
        // return $rest;
        $rest->update(['schema_res_markup' => json_encode($config)]);

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'api',
            'ref_id' => $rest->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update api : '.$rest->api_name.' step 4',
        ];
        \App\SystemLog::save_log($syslog);

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rest = ApiGateways::findOrFail($id);

        \App\ApiPartnerMap::where('api_id', $rest->id)->delete();

        $syslog = [
            'code' => 200,
            'type' => 'delete',
            'path' => 'api',
            'ref_id' => $rest->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Delete api : '.$rest->api_name,
        ];
        \App\SystemLog::save_log($syslog);

        $rest->delete();

        return redirect()->route('backend.rest.index')->with('message', 'Deleted');
    }

    public function create_pdf($rest)
    {
        // echo $rest;exit;
        // print_r(json_decode($rest->schema_req_markup));exit;
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $run = 1;
        $section = $phpWord->addSection();

        $tableStyle = array(
            'borderColor' => '000',
            'borderSize' => 1,
            'cellMargin' => '30',
        );
        $fb1 = ['color'=>'FFFFFF','size'=>'11'];
        $fb2 = ['spaceBefore'=>80,'spaceAfter'=>50,'size'=>30, 'indentation'=>['left'=>'80']];
        $f11 = ['size'=>'11'];

        if( $rest->api_type == 'private' ){
            $section->addText($run.'. Get Token Session');


            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("URLs", $fb1, $fb2);
            $table->addCell(8000)->addText('/api/auth/login', $f11, $fb2);

            $table->addRow();
            $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("Method", $fb1, $fb2);
            $table->addCell(8000)->addText('POST', $f11, $fb2);

            $section->addTextBreak(1);


            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(2000, ['bgColor'=>'0070c0', 'gridSpan' => 2])->addText('Parameter', $fb1, $fb2);

            $table->addRow();
            $table->addCell(3000)->addText("Field", $f11, $fb2);
            $table->addCell(7000)->addText('Description', $f11, $fb2);

            $table->addRow();
            $table->addCell(3000)->addText("username", $f11, $fb2);
            $table->addCell(7000)->addText('(text) Partner username', $f11, $fb2);

            $table->addRow();
            $table->addCell(3000)->addText("password", $f11, $fb2);
            $table->addCell(7000)->addText('(text) Partner password', $f11, $fb2);

            $section->addTextBreak(1);

            $run++;
        }



        $section->addText($run.'. Call API: {API_Name}');
        $section->addText('        - JSON Sample');
        $desc = $rest->api_description;
        if( !$desc )
            $desc = '-';
        $section->addText('            a. '.$desc);
        $section->addText('            b. Request url, Header, Method');



        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("URL", $fb1, $fb2);
        $table->addCell(8000)->addText('/api/'.$rest->api_url_schema, $f11, $fb2);

        if( $rest->api_type == 'private' ){
            $table->addRow();
            $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("Header", $fb1, $fb2);
            $table->addCell(8000)->addText('Authorization bearer {Token}', $f11, $fb2);
        }

        $table->addRow();
        $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("Method", $fb1, $fb2);
        $table->addCell(8000)->addText('POST', $f11, $fb2);

        $section->addTextBreak(1);


        $section->addText('            c. Parameter');

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(3000, ['bgColor'=>'0070c0', 'gridSpan' => 2])->addText('Parameter', $fb1, $fb2);

        $table->addRow();
        $table->addCell(3000)->addText("Field", $f11, $fb2);
        $table->addCell(7000)->addText('Description', $f11, $fb2);

        $req = json_decode($rest->schema_req_markup);
        
        foreach( $req->configs as $row ){
            if( $row->active == 1 ){
                $desc = $row->desc;
                if( !$desc )
                    $desc = '-';

                $table->addRow();
                $table->addCell(2000)->addText($row->rename, $f11, $fb2);
                $table->addCell(8000)->addText($desc, $f11, $fb2);
            }
        }

        $section->addTextBreak(1);


        $section->addText('            d. JSON Request Sample');

        $table = $section->addTable($tableStyle);

        $json_req = "{";
        foreach( $req->configs as $row ){
            if( $row->active == 1 )
                $json_req .= '<w:br/>    "'.$row->rename.'":"...",';
        }

        $json_req = substr($json_req, 0, -1);
        if( !$json_req )
            $json_req .= "{<w:br/>";

        $json_req .= "<w:br/>}";

        $table->addRow();
        $table->addCell(10000, ['bgColor'=>'0070c0'])->addText('Example : JSON Request', $fb1, $fb2);
        $table->addRow();
        $table->addCell(10000)->addText($json_req, $f11, $fb2);

        $section->addTextBreak(1);


        $section->addText('            e. JSON Response Sample');

        $table = $section->addTable($tableStyle);

        $res = json_decode($rest->schema_res_markup);
        // print_r($res);exit;
        $json_res = "{";
        foreach( $res as $row ){
            if( $row->active == 1 )
                $json_res .= '<w:br/>    "'.$row->rename.'":"...",';
        }

        $json_res = substr($json_res, 0, -1);
        if( !$json_res )
            $json_res .= "{<w:br/>";
        
        $json_res .= "<w:br/>}";

        $table->addRow();
        $table->addCell(10000, ['bgColor'=>'0070c0'])->addText('Example : JSON Response', $fb1, $fb2);
        $table->addRow();
        $table->addCell(10000)->addText($json_res, $f11, $fb2);

        $section->addTextBreak(1);
        $section->addTextBreak(1);


        // XML
        $section->addText($run.'. XML Sample');

        $desc = $rest->api_description;
        if( !$desc )
            $desc = '-';
        $section->addText('        f. '.$desc);
        $section->addText('        g. Request url, Header, Method');

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("URL", $fb1, $fb2);
        $table->addCell(8000)->addText('/api/'.$rest->api_url_schema.'/xml', $f11, $fb2);

        if( $rest->api_type == 'private' ){
            $table->addRow();
            $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("Header", $fb1, $fb2);
            $table->addCell(8000)->addText('Authorization bearer {Token}', $f11, $fb2);
        }

        $table->addRow();
        $table->addCell(2000, ['bgColor'=>'0070c0'])->addText("Method", $fb1, $fb2);
        $table->addCell(8000)->addText('POST', $f11, $fb2);

        $section->addTextBreak(1);


        $section->addText('        h. Parameter');

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(3000, ['bgColor'=>'0070c0', 'gridSpan' => 2])->addText('Parameter', $fb1, $fb2);

        $table->addRow();
        $table->addCell(3000)->addText("Field", $f11, $fb2);
        $table->addCell(7000)->addText('Description', $f11, $fb2);

        // $req = json_decode($rest->schema_req_markup);
        
        foreach( $req->configs as $row ){
            if( $row->active == 1 ){
                $desc = $row->desc;
                if( !$desc )
                    $desc = '-';

                $table->addRow();
                $table->addCell(2000)->addText($row->rename, $f11, $fb2);
                $table->addCell(8000)->addText($desc, $f11, $fb2);
            }
        }

        $section->addTextBreak(1);


        $section->addText('        i. XML Request Sample');

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(10000, ['bgColor'=>'0070c0'])->addText('Example : XML Request', $fb1, $fb2);

         // $req = json_decode($rest->schema_req_markup);
        $xml_req  = '&lt;?xml version="1.0" encoding="utf-8"?&gt;';
        $xml_req .= "<w:br/>&lt;".$rest->api_url_schema."&gt;";
        foreach( $req->configs as $row ){
            if( $row->active == 1 )
                $xml_req .= '<w:br/>    &lt;'.$row->rename.'&gt;...&lt;/'.$row->rename.'&gt;';
        }
        $xml_req .= "<w:br/>&lt;/".$rest->api_url_schema."&gt;";

        $table->addRow();
        $table->addCell(10000)->addText($xml_req, $f11, $fb2);

        $section->addTextBreak(1);


        $section->addText('        j. XML Response Sample');

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(10000, ['bgColor'=>'0070c0'])->addText('Example : XML Response', $fb1, $fb2);


        $xml_res  = '&lt;?xml version="1.0" encoding="utf-8"?&gt;';
        $xml_res .= "<w:br/>&lt;".$rest->api_url_schema."&gt;";
        foreach( $res as $row ){
            if( $row->active == 1 )
                $xml_res .= '<w:br/>    &lt;'.$row->rename.'&gt;...&lt;/'.$row->rename.'&gt;';
        }
        $xml_res .= "<w:br/>&lt;/".$rest->api_url_schema."&gt;";

        $table->addRow();
        $table->addCell(10000)->addText($xml_res, $f11, $fb2);




        // Save file
        $file = 'Api-Doc-'.$rest->id.'.docx';
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('docs/'.$file);
    }
}
