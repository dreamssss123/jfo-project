<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function login()
    {
    	// User::create([
     //        'name' => 'asdasdsa',
     //        'email' => 'test12@gmail.com',
     //        'password' => Hash::make('test1234'),
     //    ]);
    	return view('backend.auth.login');
    }

    public function login_submit(Request $request)
    {
    	$credentials = $request->only('email', 'password');
        
        if( Auth::attempt($credentials) )
        	return redirect()->route('backend.index');
        else
        	return redirect()->back()->with('danger', 'E-mail หรือ Password ไม่ถูกต้อง');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect()->route('backend.auth.login');
    }
}
