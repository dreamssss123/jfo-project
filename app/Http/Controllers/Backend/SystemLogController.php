<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\SystemLogDataTable;
use App\SystemLog;
use Auth;

class SystemLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SystemLogDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[10]->view ){
            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return $dataTable->render('backend.log.system_log');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export_data(Request $request)
    {
        // print_r($request->all());exit;
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $logs = SystemLog::select('system_log.id', 'system_log.status_code', 'system_log.path', 'system_log.log_type', 'system_log.txt_data', 'system_log.datetime_at', 'system_log.datetime_at as datetime_normal', 'users.firstname')
                                    ->leftJoin('users', 'system_log.user_id', 'users.id');

        if( $request->search_status )
            $logs = $logs->where('system_log.status_code', $request->search_status);
        if( $request->seach_name )
            $logs = $logs->where('system_log.path', 'LIKE', '%'.$request->seach_name.'%');
        if( $request->seach_partner )
            $logs = $logs->where('system_log.log_type', 'LIKE', '%'.$request->seach_partner.'%');

        if( $request->search_date_start && $request->search_date_end )
            $logs = $logs->whereBetween('system_log.datetime_at', [$request->search_date_start, $request->search_date_end]);
        else if( $request->search_date_start && !$request->search_date_end )
            $logs = $logs->where('system_log.datetime_at', '>', $request->search_date_start);
        else if( !$request->search_date_start && $request->search_date_end )
            $logs = $logs->where('system_log.datetime_at', '<', $request->search_date_end);

        $logs = $logs->orderBy('system_log.datetime_at', 'desc')
                        ->get();
        // return $logs;

        $columns = array('Status', 'Module', 'Type', 'Description', 'Name', 'Datetime');

        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($logs as $log) {

            $txt_data = $log->txt_data;
            if( !$txt_data )
                $txt_data = '-';

            fputcsv($file, array($log->status_code, $log->path, $log->log_type, $txt_data, $log->firstname, $log->datetime_at));
        }
        exit();
    }
}
