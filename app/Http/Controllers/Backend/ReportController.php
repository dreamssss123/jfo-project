<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\ReportDataTable;
use App\ApiLog;
use Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ReportDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[6]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'report',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'report',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export_data(Request $request)
    {
        // print_r($request->all());exit;
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // $logs = ApiLog::select('api_log.path', 'api_log.partner_name', 'api_gateway.api_name', \DB::raw('count(*) as total'))
        //                 ->leftJoin('api_gateway', 'api_log.path', 'api_gateway.api_url_schema')
        //                 ->where('api_log.log_type', 'req')
        //                 ->groupBy('api_log.path', 'api_log.partner_name', 'api_gateway.api_name')
        //                 ->get();
        $logs = ApiLog::select('api_log.path', 'api_log.partner_name', 'api_log.api_name', \DB::raw('count(*) as total'))
                        ->where(function($q){
                            $q->orWhere('api_log.log_type', 'req');
                            $q->orWhere('api_log.api_name', 'auth_login');
                        });

        if( $request->search_name )
            $logs = $logs->where('api_log.path', 'LIKE', '%'.$request->search_name.'%');
        if( $request->search_partner )
            $logs = $logs->where('api_log.partner_name', 'LIKE', '%'.$request->search_partner.'%');

        if( $request->search_date_start && $request->search_date_end )
            $logs = $logs->whereBetween('api_log.datetime_at', [$request->search_date_start, $request->search_date_end]);
        else if( $request->search_date_start && !$request->search_date_end )
            $logs = $logs->where('api_log.datetime_at', '>', $request->search_date_start);
        else if( !$request->search_date_start && $request->search_date_end )
            $logs = $logs->where('api_log.datetime_at', '<', $request->search_date_end);


        $logs = $logs->groupBy('api_log.path', 'api_log.partner_name', 'api_log.api_name')
                    ->get();
        // return $logs;
        $columns = array('API name', 'API URL', 'Partner name', 'Usage');

        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($logs as $log) {

            $name = $log->api_name;
            if( !$name )
                $name = '-';

            $partner_name = $log->partner_name;
            if( !$partner_name )
                $partner_name = '-';

            fputcsv($file, array($name, $log->path, $partner_name, $log->total));
        }
        exit();
    }
}
