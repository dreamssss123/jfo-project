<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\DatabaseConnectionDataTable;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Support\Facades\Crypt;
use App\Connection;
use Auth;

class DatabaseConnectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DatabaseConnectionDataTable $dataTable)
    {
        $perms = \App\Perm::can();
        if( !$perms[4]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'database_connection',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'database_connection',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        return $dataTable->render('backend.dbconn.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perms = \App\Perm::can();
        if( !$perms[4]->create ){

            $syslog = [
                'code' => 401,
                'type' => 'insert',
                'path' => 'database_connection',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        return view('backend.dbconn.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // print_r($request->all());exit;
        //**** Validate ****//
        $valid = Connection::validate('create', $request->conn_type);

        $validator = Validator::make($request->all(), $valid['valid'], $valid['msg']);
        // $validator->after(function ($validator) use($request) {
        //     if( $request->payment_type == 1 && $request->bank == 'undefined' )
        //         $validator->errors()->add('bank', 'กรุณาเลือกแบ้งค์ที่ท่านต้องการโอน');

        //     if( $request->hasFile('file_name') ){
        //         for( $i=0; $i<count($request->file_name); $i++ ){
        //             if( $request->other_name[$i] == '' ){
        //                 $validator->errors()->add('other_name', 'กรุณาใส่ชื่อไฟล์ให้ครบ');
        //                 break;
        //             }
        //         }
        //     }
        // });

        if( $validator->fails() )
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors(),
            ]);
        //****** End Validate *******//

        $conn = new Connection;
        $conn->user_id = Auth::user()->id;
        $conn->conn_type = $request->conn_type;
        $conn->conn_name = $request->conn_name;
        $conn->conn_alias = $request->conn_alias;
        $conn->conn_ip = $request->conn_ip;
        $conn->username = $request->username;
        $conn->password = encrypt($request->password);
        $conn->conn_port = $request->conn_port;
        $conn->conn_sid = $request->conn_sid;
        $conn->conn_database = $request->conn_database;
        $conn->conn_status = $request->conn_status;
        $conn->save();

        $syslog = [
            'code' => 200,
            'type' => 'insert',
            'path' => 'database_connection',
            'ref_id' => $conn->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Create database connection : '.$conn->conn_name,
        ];
        \App\SystemLog::save_log($syslog);

        if( $conn )
            return response()->json([
                'status' => 'success',
            ]);
        else
            return response()->json([
                'status' => 'error',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perms = \App\Perm::can();
        if( !$perms[4]->edit ){

            $syslog = [
                'code' => 401,
                'type' => 'update',
                'path' => 'database_connection',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);

            return redirect()->route('backend.index')->with('warning', "You don't have Permission.");
        }

        $user = Auth::user();
        $conn = Connection::findOrFail($id);

        if( $user->role_id != config('app.sa_id') ){
            if( $conn->user_id != $user->id ){
                $syslog = [
                    'code' => 401,
                    'type' => 'update',
                    'path' => 'database_connection',
                    'user_id' => Auth::user()->id,
                ];
                \App\SystemLog::save_log($syslog);

                return redirect()->route('backend.dbconn.index')->with('warning', "This is not your Connection.");
            }
        }

        return view('backend.dbconn.edit', compact('conn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // print_r($request->all());exit;
        //**** Validate ****//
        $valid = Connection::validate('edit', $request->conn_type);

        $validator = Validator::make($request->all(), $valid['valid'], $valid['msg']);
        // $validator->after(function ($validator) use($request) {
        //     if( $request->payment_type == 1 && $request->bank == 'undefined' )
        //         $validator->errors()->add('bank', 'กรุณาเลือกแบ้งค์ที่ท่านต้องการโอน');

        //     if( $request->hasFile('file_name') ){
        //         for( $i=0; $i<count($request->file_name); $i++ ){
        //             if( $request->other_name[$i] == '' ){
        //                 $validator->errors()->add('other_name', 'กรุณาใส่ชื่อไฟล์ให้ครบ');
        //                 break;
        //             }
        //         }
        //     }
        // });

        if( $validator->fails() )
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors(),
            ]);
        //****** End Validate *******//

        $conn = Connection::findOrFail($id);
        $conn->conn_type = $request->conn_type;
        $conn->conn_name = $request->conn_name;
        $conn->conn_ip = $request->conn_ip;
        $conn->username = $request->username;
        $conn->password = encrypt($request->password);
        $conn->conn_port = $request->conn_port;
        $conn->conn_sid = $request->conn_sid;
        $conn->conn_database = $request->conn_database;
        $conn->conn_status = $request->conn_status;
        $conn->save();

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'database_connection',
            'ref_id' => $conn->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Update database connection : '.$conn->conn_name,
        ];
        \App\SystemLog::save_log($syslog);

        if( $conn )
            return response()->json([
                'status' => 'success',
            ]);
        else
            return response()->json([
                'status' => 'error',
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conn = Connection::findOrFail($id);
        
        $syslog = [
            'code' => 200,
            'type' => 'delete',
            'path' => 'database_connection',
            'ref_id' => $conn->id,
            'user_id' => Auth::user()->id,
            'txt_data'=> 'Delete database connection : '.$conn->conn_name,
        ];
        \App\SystemLog::save_log($syslog);

        $conn->delete();

        return redirect()->route('backend.dbconn.index')->with('message', 'Deleted');
    }





    //**** Use together with other Controller ****//
    
    public function test_connect(Request $request)
    {
        // print_r($request->all());exit;
        $connect_data = [
            'driver' => $request->conn_type,
            'host' => $request->conn_ip,
            'username' => $request->username,
            'password' => $request->password,
            'database' => '',
            'port' => $request->conn_port,
        ];

        if( $request->conn_type == 'oracle' )
            $connect_data['service_name'] = $request->conn_sid;

        \config(['database.connections.custom_connect' => $connect_data]);

        try {
            \DB::connection('custom_connect')->getPdo();
            $conn = \DB::connection('custom_connect');

            $datas = [];
            if( $request->conn_type == 'sqlsrv' ){
                $conn = $conn->select('SELECT name FROM master.sys.databases');
                $datas = $conn;

            }else if( $request->conn_type == 'mysql' ){
                $dbs = $conn->select('SHOW DATABASES');
                foreach( $dbs as $db ){
                    $datas[] = [
                        'name' => $db->Database,
                    ];
                }

            }else if( $request->conn_type == 'oracle' ){
                // $dbs = $conn->select('SELECT table_name FROM user_tables');
            }

            return response()->json([
                'status' => 'success',
                'data' => $datas,
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'status' => 'error',
                'msg' => $e->getMessage(),
            ]);
        }

        // dump(\DB::select('select * from users where rownum=1'));
        // MySQL
        // \config(['database.connections.custom_connect' => [
        //     'driver' => 'mysql',
        //     'host' => 'localhost',
        //     'username' => 'root',
        //     'password' => '',
        //     'database' => '',
        //     'port' => '3306',
        // ]]);
        // return \config('database.connections.custom_connect');
        // $con = \DB::connection('custom_connect');
        // $con = $con->select('SHOW DATABASES');
        // return $con;

        // SQL Server
        // \config(['database.connections.custom_connect' => [
        //     'driver' => 'sqlsrv',
        //     'host' => 'localhost',
        //     'username' => 'sa',
        //     'password' => '123456',
        //     'database' => '',
        //     'port' => '',
        // ]]);
        // // return \config('database.connections.custom_connect');
        // $con = \DB::connection('custom_connect');
        // $con = $con->select('SELECT name FROM master.sys.databases');
        // // $con = $con->select('SELECT name FROM master.dbo.sysdatabases');
        // return $con;

        // Oracle
        // \config(['database.connections.custom_connect' => [
        //     'driver' => 'oracle',
        //     'host' => 'localhost',
        //     'username' => 'system',
        //     'password' => '1234',
        //     'database' => '',
        //     'port' => '1521',
        //     'service_name' => 'orcl',
        // ]]);
        // // return \config('database.connections.custom_connect');
        // $con = \DB::connection('custom_connect');
        // $con = $con->select('SELECT table_name FROM user_tables');
        // $con = $con->select('
        //     CREATE TABLE ofza_test2
        //     ( 
        //       customer_id number(10) NOT NULL,
        //       customer_name varchar2(50) NOT NULL,
        //       city varchar2(50),
        //       CONSTRAINT customers_pk PRIMARY KEY (customer_id)
        //     )'
        // );
        // $con = $con->table('ofza_test2')->get();
        // $con = $con->table('ofza_test2')->insert([
        //     'customer_id' => 3,
        //     'customer_name' => 'อิอิ 555+',
        // ]);
        // return $con;



        // $db = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521)))(CONNECT_DATA=(SID=orcl)))" ;

        // if($c = oci_connect("system", "1234", $db))
        // {
        //     echo "Successfully connected to Oracle.\n";
        //     OCILogoff($c);
        // }
        // else
        // {
        //     $err = OCIError();
        //     echo "Connection failed." . $err[text];
        // }
        // exit;
    }

    public function list_connection(Request $request)
    {
        // print_r( $request->all());exit;
        $user = Auth::user();

        $cons = Connection::select('conn_alias', 'conn_name');

        if( $user->role_id != config('app.sa_id') )
            $cons = $cons->where('user_id', $request->user_id);

        $cons = $cons->where('conn_type', $request->conn_type)
                    ->where('conn_status', 1)
                    ->get();
                    
        return $cons;
    }


    public function lets_connect(Request $request)
    {
        $connect_data = [
            'driver' => $request->driver,
            'host' => $request->conn_ip,
            'username' => $request->username,
            'password' => $request->password,
            'port' => $request->port,
            'database' => $request->database,
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ];

        if( $request->driver == 'oracle' )
            $connect_data['service_name'] = $request->conn_sid;


        \config(['database.connections.custom_connect' => $connect_data]);

        try {
            \DB::connection('custom_connect')->getPdo();
            $conn = \DB::connection('custom_connect');

            return [
                'status' => 'success',
                'conn' => $conn,
            ];

        } catch (\Exception $e) {

            return [
                'status' => 'error',
                'msg' => $e->getMessage(),
            ];
        }
    }

    public function connect_by_alias(Request $request)
    {
        // print_r($request->all());exit;
        $conn = Connection::where('conn_alias', $request->conn_alias)->first();
        // return $conn;

        $request = new \Illuminate\Http\Request();
        $request->replace([
            'driver' => $conn->conn_type,
            'conn_ip' => $conn->conn_ip,
            'username' => $conn->username,
            'password' => decrypt($conn->password),
            'database' => $conn->conn_database,
            'port' => $conn->conn_port,
            'conn_sid' => $conn->conn_sid,
        ]);
        // print_r($request->all());exit;
        $connect = $this->lets_connect($request);
        // print_r($connect);exit;
        if( $connect['status'] == 'success' ){

            $tbs = [];
            if( $conn->conn_type == 'mysql' ){
                $tables = $connect['conn']->select('SHOW TABLES');
                foreach( $tables as $table ){
                    foreach ($table as $key => $t)
                        $tbs[] = $t;
                }

            }else if( $conn->conn_type == 'sqlsrv' ){
                // $tables = $connect['conn']->select("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' ");
                $tables = $connect['conn']->select("SELECT TABLE_NAME,TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES ");
                foreach( $tables as $table ){
                    $tbs[] = $table->TABLE_NAME;
                }
            }else if( $conn->conn_type == 'oracle' ){
                $tables = $connect['conn']->select("SELECT table_name FROM all_tables where owner = '".$conn->username."' OR owner = '".strtoupper($conn->username)."' ");
                foreach( $tables as $table ){
                    $tbs[] = $table->table_name;
                }
            }

            return response()->json([
                'status' => 'success',
                'tables' => $tbs,
            ]);

        }else{
            return response()->json([
                'status' => 'error',
                'msg' => $connect['msg'],
            ]);
        }
    }
}
