<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Configuration;
use Auth;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perms = \App\Perm::can();
        if( !$perms[8]->view ){

            $syslog = [
                'code' => 401,
                'type' => 'view',
                'path' => 'setting',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }else{

            $syslog = [
                'code' => 200,
                'type' => 'view',
                'path' => 'setting',
                'user_id' => Auth::user()->id,
            ];
            \App\SystemLog::save_log($syslog);
        }

        $config = Configuration::where('name', 'api')->first();
        // return $config;
        return view('backend.setting.index', compact('config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $config = Configuration::where('name', 'api')->first();
        // return $config;
        if( !$config ){
            $config = new Configuration;
            $config->name = 'api';
            $config->status = 1;
        }

        $config->global_quantity = $request->global_quantity;
        $config->global_time_alert = !$request->global_time_alert ? 3 : $request->global_time_alert;
        $config->global_page_limit = !$request->global_page_limit ? 10 : $request->global_page_limit;
        $config->global_mail_error = $request->global_mail_error;
        $config->save();

        $syslog = [
            'code' => 200,
            'type' => 'update',
            'path' => 'setting',
            // 'ref_id' => $user->id,
            'user_id' => Auth::user()->id,
            // 'txt_data'=> 'Update setting',
        ];
        \App\SystemLog::save_log($syslog);

        return redirect()->route('backend.setting.index')->with('message', 'Updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
