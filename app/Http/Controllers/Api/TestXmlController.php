<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class TestXmlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $datas = User::select('id', 'name', 'email')->get();
        // \App\User::create([
        //  'firstname' => 'ABCDEF',
        //  'email' => 'abcdef@gmail.com',
        //  'password' => bcrypt('test1234'),
        //  'role_id' => 13,
        // ]);

        $er = [
            [
                'name' => 'Test1',
                'email' => 'test1@mail.com',
                'status' => 1,
            ],
            [
                'name' => 'Test2',
                'email' => 'test2@mail.com',
                'status' => 2,
            ],
        ];

        return response()->xml($er);
    }

    public function xml_test123(Request $request)
    {
        $xml = (array)simplexml_load_string($request->getContent());
        print_r($xml);
        // return response()->json($request->all());
    }

    public function test_xml12345(Request $request)
    {
        
    }

    public function res_xml($qry) // for dev
    {
        $results = \DB::select( \DB::raw($qry) );

        return response()->xml($results);
    }

    public function res_json($qry) // for dev
    {
        $results = \DB::select( \DB::raw($qry) );

        return response()->json($results);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
