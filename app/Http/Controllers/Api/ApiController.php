<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\ApiGateways;
use App\ApiLog;
use App\Configuration;

class ApiController extends Controller
{
	// public function __construct()
 //    {
 //        $this->middleware('api_auth', ['except' => ['login']]);
 //    }

    public function main(Request $request, $api='', $is_xml='')
    {
        $rest = ApiGateways::where('api_url_schema', $api)->first();
        $addition = [];
        if( $rest ){
            if( $rest->api_type == 'private' ){
                $me = $this->myme($request);
                $addition['partner'] = @$me->par_name;
            }

            $addition['api_name'] = $rest->api_name;
        }

    	$chk_xml = 0;
    	if( $is_xml || $is_xml == 'xml' )
    		$chk_xml = 1;

    	if( $chk_xml == 1 ){
    		try{
    			$get_xml = (array)simplexml_load_string($request->getContent());

    		} catch (\Exception $e) {

	            $response = response()->xml([
	                'status' => 'error',
	                'msg' => 'XML format incorrect',
	            ], 401);
                $this->save_api_log('401', $api, $response, 'res', $addition);
                return $response;
	        }

    	}else{
    		// echo $request->getContent();exit;
    		$get_xml = json_decode($request->getContent());
    		if( !$get_xml ){
    			$response = response()->json([
	                'status' => 'error',
	                'msg' => 'JSON format incorrect',
	            ], 401);
                $this->save_api_log('401', $api, $response, 'res', $addition);
                return $response;
            }
            
    	}

        $page = 1; // Set Page
        $params = [];
        foreach( $get_xml as $index => $x ){
            if( $index != 'page' )
                $params[$index] = $x;
            else
                $page = (int)$x;
        }

    	$request = new Request();
    	$request->replace($params);
        // echo $page;exit;
    	// print_r($request->all());exit;
    	$params_req = json_decode($rest->schema_req_markup);


    	// Validate Params
    	$param_incorrect = [
            'status' => 'error',
            'msg' => 'Parameter incorrect',
        ];
    	$reqs = [];
    	foreach( $params_req->configs as $p_req ){
    		if( $p_req->active == 1 )
    			$reqs[$p_req->rename] = 0;
    	}
    	// print_r($reqs);exit;

    	foreach( $params as $key => $row ){
    		if( isset($reqs[$key]) ){
    			$reqs[$key] = 1;
    		}else{
    			if( $chk_xml == 1 )
    				$response = response()->xml($param_incorrect, 401);
                else
    				$response = response()->json($param_incorrect, 401);

                $this->save_api_log('401', $api, $response, 'res', $addition);
                return $response;
    		}
    	}

    	foreach( $reqs as $rq ){
    		if( !$rq || $rq == 0 ){
    			if( $chk_xml == 1 )
    				$response = response()->xml($param_incorrect, 401);
    			else
    				$response = response()->json($param_incorrect, 401);

                $this->save_api_log('401', $api, $response, 'res', $addition);
                return $response;
    		}
    	}
    	// End Validate Params

    	// exit;
    	$conn = $rest->connection;
    	// return $conn;

    	$dbconn = new \App\Http\Controllers\Backend\DatabaseConnectionController;
    	$req = new Request();
        $req->replace([
            'driver' => $conn->conn_type,
            'conn_ip' => $conn->conn_ip,
            'username' => $conn->username,
            'password' => decrypt($conn->password),
            'database' => $conn->conn_database,
            'port' => $conn->conn_port,
            'conn_sid' => $conn->conn_sid,
        ]);
        // print_r($req->all());exit;
        $connect = $dbconn->lets_connect($req);
        // $columns = $connect['conn']->getSchemaBuilder()->getColumnListing($rest->conn_table);
        // return $columns;


        /***** Select Data *******/
        $config = Configuration::where('name', 'api')->first();
        $rest_req = json_decode($rest->schema_req_markup);
        // print_r($rest_req);
        $query = $connect['conn']->table($rest->conn_table);

        foreach( $rest_req->configs as $row ){

        	if( $row->active == 1 && $request->has($row->rename) ){

        		$search_val = $request[$row->rename];

        		if( $row->operator == 'equal' )
	        		$opr = '=';
	        	else if( $row->operator == 'not_equal' )
	        		$opr = '!=';
	        	else if( $row->operator == 'greate_than' )
	        		$opr = '>';
	        	else if( $row->operator == 'less_than' )
	        		$opr = '<';
	        	else if( $row->operator == 'greate_equal' )
	        		$opr = '>=';
	        	else if( $row->operator == 'less_equal' )
	        		$opr = '<=';
	        	else if( $row->operator == 'like' ){
	        		$opr = 'LIKE';
	        		$search_val = '%'.$request[$row->rename].'%';
	        	}else if( $row->operator == 'not_like' ){
	        		$opr = 'NOT LIKE';
	        		$search_val = '%'.$request[$row->rename].'%';
	        	}

	        	if( $rest_req->condition == 'and' )
		        	$query = $query->where($row->name, $opr, $search_val);
		        else
		        	$query = $query->orWhere($row->name, $opr, $search_val);
        	}
        }
        // exit;

        $count_all = $query->count();
        // echo $count_all;exit;

        // Set Pagination
        $limit = 10;
        if( $config && $config->global_page_limit )
            $limit = $config->global_page_limit;

        $offset = ($page-1)*$limit;
        $query = $query->offset($offset)
                        ->limit($limit);

        $query = $query->get();
        // return $query;
        /***** End Select Data *******/


        /***** Response Data ********/
        $rest_res = json_decode($rest->schema_res_markup);
        // print_r($rest_res);exit;
        $response_data = [];
        foreach( $query as $row ){
        	// print_r($row);
        	$data_row = [];
        	foreach( $rest_res as $key => $res ){
        		if( $res->active == 1 )
                    // echo $row->$key.' - '.$key."\n";
                    if( !isset($row->$key) )
                        $key = strtolower($key);

                    if( $res->rename != '' )
        			     $data_row[$res->rename] = mb_convert_encoding($row->$key, 'UTF-8', 'UTF-8');
        	}

        	$response_data[] = $data_row;
        }

        /***** End Response Data ********/

        $new_response = [
            'status' => 'success',
            'total' => $count_all,
            'per_page' => $limit,
            'data' => $response_data,
        ];
        // print_r($response_data);exit;
        if( $chk_xml == 1 )
        	$response = response()->xml($new_response);
        else
        	$response = response()->json($new_response);
        
        $this->save_api_log('200', $api, $response, 'res', $addition);

        // return $response->header('X-Header-total', $count_all);
        return $response;
    }

    public function save_api_log($code, $api, $txt_data, $log_type, $addition=[])
    {
        $log = new ApiLog;
        $log->status_code = $code;
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->path = $api;
        $log->api_name = isset($addition['api_name']) ? $addition['api_name'] : '';
        $log->agent = $_SERVER['HTTP_USER_AGENT'];
        $log->partner_name = isset($addition['partner']) ? $addition['partner'] : '';
        $log->datetime_at = Carbon::now();
        $log->methods = 'post';
        $log->txt_data = $txt_data;
        $log->log_type = $log_type;
        $log->status = 1;
        $log->save();

        
        if( $code != '200' && $code != '404' ){
            $c = new Carbon;
            $check_dup = Configuration::where('name', 'api/'.$api)->first();
            if( !$check_dup ){
                $cd = new Configuration;
                $cd->name = 'api/'.$api;
                $cd->global_quantity = 1;
                $cd->global_time_alert = 0;
                $cd->Status = 1;
                $cd->Save();
            }else{

                $setting = Configuration::where('name', 'api')->first();
                $qty = (int)$check_dup->global_quantity;
                $qty++;
                if( $setting && $setting->global_quantity && $qty >= $setting->global_quantity ){

                    $send_mail = 1;
                    if( $check_dup->description ){
                        $now = $c->now();
                        $last_time = $c->parse($check_dup->description);
                        $diff = $now->diffInMinutes($last_time);

                        if( $setting->global_time_alert > (int)$diff )
                            $send_mail = 0;
                    }
                    // echo $setting->global_time_alert.' '.$diff.' sss '.$send_mail;
                    // exit;

                    if( $send_mail == 1 ){
                        Mail::send('emails.alert', ['api' => $api], function($message) use($setting) {
                            $message->to($setting->global_mail_error, 'JFO API Gateway')
                                    ->subject('Api request reject over limit');
                            $message->from('jfo@info.com');
                        });
                        $qty = 0;
                        $check_dup->description = $c->now();
                    }
                }
                
                $check_dup->global_quantity = $qty;
                $check_dup->save();
            }
        }
    }


    public function login()
    {
        $addition['api_name'] = 'auth_login';
        $this->save_api_log('200', 'auth/login', 'username: '.request('username'), 'req', $addition);

    	$credentials = request(['username', 'password']);
        // return $credentials;
        if (! $token = auth('api')->setTTL(60)->attempt($credentials)) {
            $response = response()->json([
            	'status' => 'error',
            	'msg' => 'Unauthorized',
            ], 401);
            $this->save_api_log('401', 'auth/login', $response, 'res');
            return $response;
        }

        $partner = auth('api')->user();
        if( $partner && $partner->status == 0 ){
            $response = response()->json([
                'status' => 'error',
                'msg' => 'Partner was disabled',
            ], 403);
            $this->save_api_log('403', 'auth/login', $response, 'res');
            return $response;
        }

        $response = $this->respondWithToken($token);
        $this->save_api_log('200', 'auth/login', $response, 'res', $addition);
        return $response;
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    public function myme(Request $request)
    {
        return auth('api')->user();
    }

    public function me(Request $request)
	{
        return response()->json(auth('api')->user());
    }

 //    public function logout()
 //    {
 //        auth('api')->logout();

 //        return response()->json(['message' => 'Successfully logged out']);
 //    }

 //    public function refresh()
 //    {
 //        return $this->respondWithToken(auth('api')->refresh());
 //    }

}
