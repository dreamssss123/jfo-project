<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !Auth::check() )
            return redirect()->route('backend.auth.login');

        /*** Set Permission ***/
        $user = Auth::user();
        if( !$user->status ){
            Auth::logout();
            return redirect()->route('backend.auth.login')->with('danger', 'อีเมล์ของคุณถูกระงับการใช้งาน');
        }

        $role = $user->role;

        // Force Logout
        if( !$role || !$role->status ){
            Auth::logout();
            return redirect()->route('backend.auth.login')->with('danger', 'สิทธ์ของท่านไม่สามารถเข้าใช้ระบบได้');
        }

        // Set Permission
        $perms = \App\Perm::select('menu_id', 'can_view', 'can_create', 'can_edit', 'can_delete')
                        ->where('role_id', $role->id)
                        ->get();
        $data_perms = [];
        foreach( $perms as $perm ){
            $data_perms[$perm->menu_id] = (object) [
                'view' => $perm->can_view,
                'create' => $perm->can_create,
                'edit' => $perm->can_edit,
                'delete' => $perm->can_delete,
            ];
        }
        // echo $perms;exit;
        session()->put('perms', $data_perms);

        return $next($request);
    }
}
