<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Illuminate\Http\Request;
use App\ApiGateways;
use App\ApiLog;

class ApiAuth extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $me = $this->me($request);
        $schema = $request->route()->parameter('schema');
        $is_xml = $request->route()->parameter('is_xml');
        $chk_xml = 0;
        if( $is_xml || $is_xml == 'xml' )
            $chk_xml = 1;

        $addition = [];
        $api = ApiGateways::where('api_url_schema', $schema)->first();
        if( $api ){
            if( $api->api_type == 'private' ){
                $me = $this->myme($request);
                $addition['partner'] = @$me->par_name;
            }

            $addition['api_name'] = $api->api_name;
        }

        $api_cont = new \App\Http\Controllers\Api\ApiController;
        $api_cont->save_api_log('200', $schema, $request->getContent(), 'req', $addition);

        if( !$api ){
            $param = [
                'status' => 'error',
                'msg' => 'API not found',
            ];
            if( $chk_xml == 1 )
                $response = response()->xml($param, 404);
            else
                $response = response()->json($param, 404);
            $api_cont->save_api_log('404', $schema, $response, 'res', $addition);
            return $response;
        }

        if( $api->status == 0 ){
            $param = [
                'status' => 'error',
                'msg' => 'API was disabled',
            ];
            if( $chk_xml == 1 )
                $response = response()->xml($param, 403);
            else
                $response = response()->json($param, 403);
            $api_cont->save_api_log('403', $schema, $response, 'res', $addition);
            return $response;
        }

        if( !$api->connection ){
            $param = [
                'status' => 'error',
                'msg' => 'Database Connection not found',
            ];
            if( $chk_xml == 1 )
                $response = response()->xml($param, 401);
            else
                $response = response()->json($param, 401);
            $api_cont->save_api_log('401', $schema, $response, 'res', $addition);
            return $response;

        }else{
            if( $api->connection->conn_status != 1 ){
                $param = [
                    'status' => 'error',
                    'msg' => 'Database Connection was disable',
                ];
                if( $chk_xml == 1 )
                    $response = response()->xml($param, 403);
                else
                    $response = response()->json($param, 403);
                $api_cont->save_api_log('403', $schema, $response, 'res', $addition);
                return $response;
            }
        }

        if( $api->api_type == 'private' ){
            try {
                $user = JWTAuth::parseToken()->authenticate();

            } catch (Exception $e) {
                $error = ['status' => 'error'];
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
                    $error['msg'] = 'Token is Invalid';
                else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
                    $error['msg'] = 'Token is Expired';
                else
                    $error['msg'] = 'Authorization Token not found';
                
                if( $chk_xml == 1 )
                    $response = response()->xml($error, 401);
                else
                    $response = response()->json($error, 401);

                $api_cont->save_api_log('401', $schema, $response, 'res', $addition);
                return $response;
            }

            // Partner
            $partner = auth('api')->user();
            if( $partner->status == 0 ){
                $param = [
                    'status' => 'error',
                    'msg' => 'Partner was disable',
                ];
                if( $chk_xml == 1 )
                    $response = response()->xml($param, 403);
                else
                    $response = response()->json($param, 403);
                $api_cont->save_api_log('403', $schema, $response, 'res', $addition);
                return $response;
            }
            // Partner Organize
            if( !$partner->partner_org || $partner->partner_org->status == 0 ){
                $param = [
                    'status' => 'error',
                    'msg' => 'Your Partner Organize was disable or delete',
                ];
                if( $chk_xml == 1 )
                    $response = response()->xml($param, 403);
                else
                    $response = response()->json($param, 403);
                $api_cont->save_api_log('403', $schema, $response, 'res', $addition);
                return $response;
            }



            // echo $api;
            // echo auth('api')->user()->api_map;
            $use_api = 0;
            foreach( auth('api')->user()->api_map as $map ){
                if( $map->api_id == $api->id )
                    $use_api++;
            }

            if( $use_api == 0 ){
                $param = [
                    'status' => 'error',
                    'msg' => 'Cannot access this API',
                ];
                if( $chk_xml == 1 )
                    $response = response()->xml($param, 401);
                else
                    $response = response()->json($param, 401);

                $api_cont->save_api_log('401', $schema, $response, 'res', $addition);
                return $response;
            }
        }


        if( !$api->schema_req_markup || !$api->schema_res_markup ){
            $param = [
                'status' => 'error',
                'msg' => 'API incomplete',
            ];
            if( $chk_xml == 1 )
                $response = response()->xml($param, 401);
            else
                $response = response()->json($param, 401);

            $api_cont->save_api_log('401', $schema, $response, 'res', $addition);
            return $response;
        }

        return $next($request);
    }

    public function myme(Request $request)
    {
        // return response()->json(auth('api')->user());
        return auth('api')->user();
    }
}
