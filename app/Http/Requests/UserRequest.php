<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $u = new \App\User;
        $request = $this->input();
        // print_r($request);exit;

        $validate = [
            'firstname' => 'required',
            'role_id' => 'required',
        ];

        if( !isset(request()->segments()[2]) )
            $validate['email'] = 'required|unique:users';

        if( $request['password'] == $u->blank_password )
            $validate['password'] = 'min:6';
        else
            $validate['password'] = 'required|confirmed|min:6';
        
        return $validate;
    }
}
